# Critères d'évaluations enseignants


- Conception 
    - Cible d’utilisateurs claire
    - Analyse des besoins (entretiens avec des utilisateurs potentiels)
    - Cas d’utilisations spécifiés pour chaque fonctionnalités (format user story)
    - Tests utilisateurs

- Technique
    - Web
    - Persistence
    - Packaging
    - Déploiement VM
    - Runner Gitlab
    - SonarQube

- Suivi et gestions de projet
    - Livrables    (S1, S2, S3, S4)
    - Structuration du groupe 
    - Méthode de GL utilisées 
    - Outils mis en place 
    - Retour sur ce qui a marché / pas marché

- Qualité
    - Qualité du rendu final / finitions
    - Qualité du code final 
    - Process de qualité

- Architecture
    - Explications / Schéma global 
    - Justification du choix des frameworks

- Présentation et Démonstration            
    - Fluidité et clarté de la présentation 
    - Fluidité et clarté de la démo
    - Fonctionnalités principales 
    - Atouts du produit 
    - Fonctionnement
    - Réponses aux questions
