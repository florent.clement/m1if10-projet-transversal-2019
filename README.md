
# M1IF10 Projet Transversal 2019-2020

AmphiQuest is a web application that allows you to create multiple choice questions, share them via QRcode and let the users fill them out online.

The results are displayed in real time as histograms and you can keep your MCQs by creating an account.

![screenshot](https://forge.univ-lyon1.fr/p1601511/m1if10-projet-transversal-2019/raw/master/capture.png)

# Prerequisites

 - JDK 11
 - Maven
 - A database (MariaDB).
 - A HTTP webserver (Tomcat).

### JDK 11 

https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html 

### Maven

The project works with a [pom](/pom.xml) and need Maven to be installed.
You can get Maven at https://maven.apache.org/

### Installation base de données

To make the website work a database is **required**.

## Installing MariaDB

If you encounter difficulties installing MariaDB on Linux you can try locally :

```
sudo apt remove --purge mysql*
sudo apt autoremove
sudo apt install mariadb-server
sudo mysql_secure_installation
sudo service mysql restart

sudo mysql -u root

USE mysql;
CREATE USER 'user'@'localhost' IDENTIFIED BY 'your_password';
GRANT ALL PRIVILEGES ON *.* TO 'user'@'localhost';
UPDATE user SET plugin="mysql_native_password" WHERE User='user'; 
FLUSH PRIVILEGES;
exit;
```

# Website installation

The SQL queries to create the tables are in m1if10.sql.

```
mvn install -Ptest-mariadb
```

# Deployment

```
mvn compile
mvn tomcat:deploy
```

# Project content

## Files tree
    root
	|- livrables
    |  |- Documents_architecture_logiciel.pdf
    |  |- Documents_etude_preliminaire.pdf
	|- src
    |  |- main
    |  |  |- config
    |  |  |  |- checkstyle.xml
    |  |  |- java
    |  |  |  |- fr
    |  |  |  |  |- univ
    |  |  |  |  |  |- lyon1
    |  |  |  |  |  |  |- context
    |  |  |  |  |  |  |- dao
    |  |  |  |  |  |  |- dto
    |  |  |  |  |  |  |- filter
    |  |  |  |  |  |  |- manager
    |  |  |  |  |  |  |- model
    |  |  |  |  |  |  |- web
    |  |  |- resources
    |  |  |  |- META-INF
    |  |  |- webapp
    |  |  |  |- ressources
    |  |  |  |- WEB-INF
    |  |- test
    |  |  |- java
    |  |  |  |- fr
    |  |  |  |  |- univ
    |  |  |  |  |  |- lyon1
    |  |  |  |  |  |  |- dao
    |  |  |  |  |  |  |- dto
    |  |  |- java
    |- .gitignore
    |- .gitlab-ci.yml
    |- README.md
    |- m1if10.sql
    |- pom.xml
    |- setup-mvn-proxy.sh
    |- sonar-project.properties 
    
## Content description

- **[livrables](/livrables)** project deliverables.
- **gitignore**
- **gitlab-ci.yml** unit tests configuration file.
- **m1if10.sql** database installation file.
- **pom.xml** project management file.
- **setup-mvn-proxy.sh** execution file in order to download maven on gitlab-ci.
- **sonar-project.properties** sonarqube propertie file.


# Virtual Machine

You can access directly to the website via port 8080 :
http://192.168.74.216:8080

Or via proxy pass (port 80) :
http://192.168.74.216/api

# Wiki

The wiki contains :
 * Deliverables including the preliminary study report and the software architecture document.
 * Meeting reports.
 * Uses case.

 # Contributors

- Virgile
- Sam
- Nabil
- Florent
- Pablo
- Jose