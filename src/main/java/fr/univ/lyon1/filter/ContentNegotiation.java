package fr.univ.lyon1.filter;

import fr.univ.lyon1.context.JsonStrategy;
import fr.univ.lyon1.context.ResourcesStrategy;
import fr.univ.lyon1.context.XmlStrategy;
import fr.univ.lyon1.dto.DTO;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;

@WebFilter(filterName = "ContentNegotiation")
public class ContentNegotiation implements Filter {

    FilterConfig filterConfig;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }


    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {

        chain.doFilter(req, resp);

        String view = (String) req.getAttribute("view");
        // S'il y a un contenu à renvoyer
        if (view != null) {
            DTO dto = (DTO) ((HttpServletRequest) req).getAttribute("dto");
            if(dto != null) {
                req.setAttribute("model",dto.build());
            }
            

            if (((HttpServletRequest) req).getHeader("Accept").startsWith("text/html")) {
                // Cas des JSP (nommées dans le web.xml)
                RequestDispatcher dispatcher = filterConfig.getServletContext().getNamedDispatcher(view);
                HttpServletRequest wrapped = new HttpServletRequestWrapper((HttpServletRequest) req) {
                    @Override
                    public String getServletPath() {
                        return "";
                    }
                };
                dispatcher.forward(wrapped, resp);

            } else if (((HttpServletRequest) req).getHeader("Accept").startsWith("application/xml")) {
                ResourcesStrategy resourcesStrategy = new XmlStrategy();
                resp.setCharacterEncoding("UTF-8");
                resp.setContentType("application/xml");
                resp.getOutputStream().print(resourcesStrategy.getContentPage(dto));

            } else {
                ResourcesStrategy resourcesStrategy = new JsonStrategy();
                resp.setCharacterEncoding("UTF-8");
                resp.setContentType("application/json");
                resp.getOutputStream().print(resourcesStrategy.getContentPage(dto));
            }
        }

    }
}
