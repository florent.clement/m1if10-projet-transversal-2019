package fr.univ.lyon1.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity(name = "qcm")
@NamedNativeQueries(value = {
        @NamedNativeQuery(
                name = "qcm.findById",
                query = "select q from qcm q where q.id = :id",
                resultClass = Qcm.class
        ),
        @NamedNativeQuery(
                name = "qcm.getQcmIdList",
                query = "SELECT qcm.* FROM qcm WHERE qcm.id IN (SELECT users_qcm.id_qcm FROM users_qcm WHERE users_qcm.name_users = ?0)",
                resultClass = Qcm.class
        )
})
public class Qcm {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "titre",nullable = false)
    private String titre;

    @OneToMany(targetEntity = Question.class, mappedBy = "qcm",cascade = CascadeType.ALL,fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Question> questionList = new ArrayList<>();

    public Qcm() {
        this.titre = "";
    }

    public Qcm(String titre) {
        this.titre = titre;
    }

    public Qcm(String titre, List<Question> questionList) {
        this.titre = titre;
        this.questionList = questionList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitre() { return titre; }

    public void setTitre(String titre) { this.titre = titre; }

    public List<Question> getQuestionList() {
        return questionList;
    }

    public void addQuestion(Question question) {
        question.setQcm(this);
        this.questionList.add(question);
    }

    public void setQuestionList(List<Question> questionList) {
        this.questionList = questionList;
    }

    @Override
    public int hashCode() {
        return Math.toIntExact(this.id) + this.titre.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Qcm qcm = (Qcm) o;
        return Objects.equals(id, qcm.id) &&
                Objects.equals(questionList, qcm.questionList);
    }
}
