package fr.univ.lyon1.model;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "users")
public class Users {
    @Id
    private String name;

    @Column(name = "password")
    private int password;

    public Users(String name, int password) {
        this.name = name;
        this.password = password;
    }

    public Users() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPassword() {
        return password;
    }

    public void setPassword(int password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode() + this.password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Users user = (Users) o;
        return Objects.equals(name, user.name) &&
                Objects.equals(password, user.password);
    }
}
