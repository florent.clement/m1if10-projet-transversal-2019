package fr.univ.lyon1.model;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "choice")
public class Choice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(targetEntity = Question.class)
    @JoinColumn(name = "id_question")
    private Question question;

    @Column(name = "content")
    private String content;

    @Column(name = "counter")
    private int counter;

    public Choice(Question question, String content, int counter) {
        this.question = question;
        this.content = content;
        this.counter = counter;
    }

    public Choice() {
        this.content = "";
        this.counter = 0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public void incrementCounter() {
        counter += 1;
    }

    @Override
    public int hashCode() {
        return Math.toIntExact(this.id) + this.content.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Choice choice = (Choice) o;
        return counter == choice.counter &&
                Objects.equals(id, choice.id) &&
                Objects.equals(question, choice.question) &&
                Objects.equals(content, choice.content);
    }
}
