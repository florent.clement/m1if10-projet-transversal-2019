package fr.univ.lyon1.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity(name = "question")
@Table(name = "question")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "content")
    private String content;

    @ManyToOne(targetEntity = Qcm.class)
    @JoinColumn(name = "id_qcm")
    private Qcm qcm;

    @OneToMany(targetEntity = Choice.class, mappedBy = "question",cascade = CascadeType.ALL,fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Choice> choiceList = new ArrayList<>();

    public Question(Qcm qcm, String content) {
        this.qcm = qcm;
        this.content=content;
    }

    public Question() {
        this.content = "";
    }

    public Question(Qcm qcm, String content, List<Choice> choiceList) {
        this.qcm = qcm;
        this.content=content;
        this.choiceList = choiceList;
    }

    public void addChoice(Choice c) {
        c.setQuestion(this);
        choiceList.add(c);
    }

    public List<Choice> getChoiceList() {
        return choiceList;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Qcm getQcm() {
        return qcm;
    }

    public void setQcm(Qcm qcm) {
        this.qcm = qcm;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setChoiceList(List<Choice> choiceList) {
        this.choiceList = choiceList;
    }

    @Override
    public int hashCode() {
        return Math.toIntExact(this.id) + this.content.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question = (Question) o;
        return Objects.equals(id, question.id) &&
                Objects.equals(content, question.content) &&
                Objects.equals(qcm, question.qcm) &&
                Objects.equals(choiceList, question.choiceList);
    }
}
