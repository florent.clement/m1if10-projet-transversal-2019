package fr.univ.lyon1.web;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

@WebServlet(name = "router")
public class Router extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(Router.class.getName());

    private void choiceRouter(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            req.getServletContext().getNamedDispatcher("choice").include(req, resp);
        }
        catch (Exception e) {
            LOGGER.throwing(Router.class.getName(),"choiceRouter",e);
        }
    }

    private void questionsRouter(String url, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // Vérifie qu'un choix qui est demandé
        if (Regex.CHOICE_ROUTER.matcher(url).matches() ||
                Regex.CHOICE.matcher(url).matches() ||
                Regex.CHOICE_ID.matcher(url).matches()) {
            this.choiceRouter(req, resp);
        } else {
            try {
                req.getServletContext().getNamedDispatcher("question").include(req, resp);
            }
            catch (Exception e) {
                LOGGER.throwing(Router.class.getName(),"questionsRouter",e);
            }
        }
    }

    private void qcmRouter(String url, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // Vérifie que c'est une question qui est demandé
        if (Regex.QUESTIONS_ROUTER.matcher(url).matches() ||
                Regex.QUESTIONS.matcher(url).matches() ||
                Regex.QUESTIONS_ID.matcher(url).matches()) {
            this.questionsRouter(url, req, resp);
        } else {
            try {
                req.getServletContext().getNamedDispatcher("qcm").include(req, resp);
            }
            catch (Exception e) {
                LOGGER.throwing(Router.class.getName(),"qcmRouter",e);
            }
        }
    }

    private void loginRouter(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            req.getServletContext().getNamedDispatcher("login").forward(req, resp);
        }
        catch (Exception e) {
            LOGGER.throwing(Router.class.getName(),"loginRouter",e);
        }
    }

    private void decoRouter(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            req.getServletContext().getNamedDispatcher("deco").forward(req, resp);
        }
        catch (Exception e) {
            LOGGER.throwing(Router.class.getName(),"decoRouter",e);
        }
    }

    private void userRouter(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            req.getServletContext().getNamedDispatcher("user").forward(req, resp);
        }
        catch (Exception e) {
            LOGGER.throwing(Router.class.getName(),"userRouter",e);
        }
    }

    private void sendRequest(String url, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (url.equals("/")) {
            req.getServletContext().getNamedDispatcher("home").forward(req, resp);
        } else if (Regex.QCM_ROUTER.matcher(url).matches() || Regex.QCM.matcher(url).matches()
                || Regex.QCM_ID.matcher(url).matches() || Regex.QCM_EDIT.matcher(url).matches()) {

            this.qcmRouter(url, req, resp);

        } else if (Regex.RESSOURCES.matcher(url).matches() || Regex.ASSETS.matcher(url).matches()) {
            // Ressources ( CSS / JS / etc... )
            HttpServletRequest wrapped = new HttpServletRequestWrapper((HttpServletRequest) req) {
                @Override
                public String getServletPath() {
                    return "";
                }
            };
            req.getRequestDispatcher(url).forward(wrapped, resp);

        } else if (Regex.LOGIN_ROUTER.matcher(url).matches() || Regex.LOGIN.matcher(url).matches()) {
            this.loginRouter(req, resp);

        } else if (Regex.DECO_ROUTER.matcher(url).matches() || Regex.DECO.matcher(url).matches()) {
            this.decoRouter(req, resp);

        } else if (Regex.USER_ROUTER.matcher(url).matches() || Regex.USER.matcher(url).matches()) {
            this.userRouter(req, resp);
        }
    }

    public static String getUrl(HttpServletRequest req) {
        return req.getRequestURI().substring(req.getContextPath().length());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
        this.sendRequest(getUrl(req), req, resp);
        } catch (Exception e) {
            LOGGER.throwing(Router.class.getName(),"doGet",e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
        this.sendRequest(getUrl(req), req, resp);
        } catch (Exception e) {
            LOGGER.throwing(Router.class.getName(),"doPost",e);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
        this.sendRequest(getUrl(req), req, resp);
        } catch (Exception e) {
            LOGGER.throwing(Router.class.getName(),"doPut",e);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            this.sendRequest(getUrl(req), req, resp);
        } catch (Exception e) {
            LOGGER.throwing(Router.class.getName(),"doDelete",e);
        }
    }
}
