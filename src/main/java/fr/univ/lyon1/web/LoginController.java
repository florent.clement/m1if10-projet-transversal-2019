package fr.univ.lyon1.web;

import fr.univ.lyon1.dao.DAOContext;
import fr.univ.lyon1.dao.UsersDAO;
import fr.univ.lyon1.manager.JwtManager;
import fr.univ.lyon1.model.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.logging.Logger;

@WebServlet(name="login")
public class LoginController extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(LoginController.class.getName());

    @Override
    public void init() throws ServletException {
        DAOContext.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession userSesh = req.getSession(false);
        String subject = null;
        if (userSesh != null) {
            subject = JwtManager.getTokenSubject(userSesh);
        }
        try {
            if (subject != null) {
                    resp.sendRedirect(req.getContextPath() + "/user");
            } else {
                String action = req.getParameter("action");
                if (action != null && action.equals("create")) {
                        req.getRequestDispatcher("/WEB-INF/login-create.jsp").forward(req, resp);

                } else {
                        req.getRequestDispatcher("/WEB-INF/login-display.jsp").forward(req, resp);
                }
            }
        } catch(Exception e) {
            LOGGER.throwing(LoginController.class.getName(),"doGet",e);
        }
    }

    private static boolean doUser(boolean create, Users user) {
        if (create) {
            // Send model to data base
            // If false then username exists
            return UsersDAO.create(user);
        } else {
            // Get model from data base
            Users userInDB = UsersDAO.getByName(user.getName());
            // If false Wrong password
            return user.equals(userInDB);
        }
    }

    private static void doToken(HttpServletRequest request, String username) {
        HttpSession userSesh = request.getSession(false);
        if (userSesh == null) {
            userSesh = request.getSession(true);
        }
        //Create JWT Token
        JwtManager tokenManager = new JwtManager(username);
        tokenManager.createOrUpdateSessionToken(userSesh);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Test question attribute.
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String action = request.getParameter("action");
        try {
            if (username != null && !username.equals("") && password != null && !password.equals("")) {

                // Generate model Users
                Users user = new Users(username, password.hashCode());
                boolean create = action != null && action.equals("create");

                if (doUser(create, user)) {
                    doToken(request, username);
                    response.sendRedirect(request.getContextPath() + "/user");
                } else {
                    if (create) {
                        response.sendRedirect(request.getContextPath() + "/login?action=create&exists=true");

                    } else {
                        response.sendRedirect(request.getContextPath() +"/login?wrong=true");

                    }
                }

            } else {
                LOGGER.warning("POST request without correct Parameters.");
                response.sendRedirect(request.getContextPath() + "/login");
            }
        }
        catch(Exception e) {
            LOGGER.throwing(LoginController.class.getName(),"doPost",e);
        }
    }
}
