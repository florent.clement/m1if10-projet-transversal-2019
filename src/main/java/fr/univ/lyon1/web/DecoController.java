package fr.univ.lyon1.web;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.logging.Logger;

@WebServlet(name= "deco")
public class DecoController extends HttpServlet{

    private static final Logger LOGGER = Logger.getLogger(DecoController.class.getName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            HttpSession userSesh = req.getSession(false);
            if (userSesh != null) userSesh.invalidate();
            resp.sendRedirect(req.getContextPath() + "/");
        }
        catch (Exception e) {
            LOGGER.throwing(DecoController.class.getName(),"doGet",e);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            resp.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
        }
        catch(Exception e) {
            LOGGER.throwing(DecoController.class.getName(),"doPost",e);
        }
    }
}
