package fr.univ.lyon1.web;

import fr.univ.lyon1.dao.DAOContext;
import fr.univ.lyon1.dao.UsersDAO;
import fr.univ.lyon1.dao.UsersQcmDAO;
import fr.univ.lyon1.manager.JwtManager;
import fr.univ.lyon1.model.Qcm;
import fr.univ.lyon1.model.Users;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

@WebServlet(name = "user")
public class UserController extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(UserController.class.getName());

    @Override
    public void init() throws ServletException {
        DAOContext.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            HttpSession userSesh = req.getSession(false);
            if (userSesh != null) {
                String username = JwtManager.getTokenSubject(userSesh);
                if (username != null) {
                    Users user = UsersDAO.getByName(username);
                    if (user != null) {
                        req.setAttribute("username", username);
                        List<Qcm> qcmList = UsersQcmDAO.getQcmIdList(username);
                        if (qcmList != null && !qcmList.isEmpty()) {
                            req.setAttribute("qcmList", qcmList);
                        }
                        req.getRequestDispatcher("/WEB-INF/user-page.jsp").forward(req, resp);
                        return;
                    } else {
                        resp.sendRedirect(req.getContextPath() + "/deco");
                        return;
                    }
                }
            }
            resp.sendRedirect(req.getContextPath() + "/login");
        } catch (Exception e) {
            LOGGER.throwing(UserController.class.getName(),"doGet",e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
    }
}
