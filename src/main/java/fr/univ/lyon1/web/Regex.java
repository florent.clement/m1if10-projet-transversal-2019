package fr.univ.lyon1.web;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public enum Regex {


    HOME("^/?"),

    RESSOURCES("^/ressources/*"),
    ASSETS("^/assets/*"),

    LOGIN_ROUTER("^/login/*"),
    LOGIN("^/login/?"),

    QCM_ROUTER ("^/qcm/*"),
    QCM ("^/qcm/?"),
    QCM_ID("^/qcm/([0-9]+)/?"),
    QCM_EDIT("^/qcm/([0-9]+)/edit/?"),

    QUESTIONS_ROUTER("^/qcm/([0-9]+)/questions/*"),
    QUESTIONS("^/qcm/([0-9]+)/questions/?"),
    QUESTIONS_ID("^/qcm/([0-9]+)/questions/([0-9]+)/?"),

    CHOICE_ROUTER("^/qcm/([0-9]+)/questions/([0-9]+)/choices/*"),
    CHOICE("^/qcm/([0-9]+)/questions/([0-9]+)/choices/?"),
    CHOICE_ID("^/qcm/([0-9]+)/questions/([0-9]+)/choices/([0-9]+)/?"),

    USER_ROUTER("^/user/.*"),
    USER("^/user/?"),

    DECO_ROUTER("^/deco/*"),
    DECO("^/deco/?");


    private Pattern pattern;

    Regex(String pattern){
        this.pattern = Pattern.compile(pattern);
    }

    public Matcher matcher(String url) {
        return pattern.matcher(url);
    }
}
