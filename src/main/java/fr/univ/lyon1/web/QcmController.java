package fr.univ.lyon1.web;

import fr.univ.lyon1.context.BuilderStrategy;
import fr.univ.lyon1.dao.*;
import fr.univ.lyon1.dto.BuilderDTO;
import fr.univ.lyon1.dto.QcmDTO;
import fr.univ.lyon1.manager.JwtManager;
import fr.univ.lyon1.model.Choice;
import fr.univ.lyon1.model.Qcm;
import fr.univ.lyon1.model.Question;
import org.modelmapper.internal.Pair;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

@WebServlet(name = "qcm")
public class QcmController extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(QcmController.class.getName());

    private static final String DISPLAY = "qcm-display";

    @Override
    public void init() throws ServletException {
        DAOContext.init();
    }

    private static Long getIdQCM(String url) {
        Matcher matcher = Regex.QCM_ID.matcher(url);
        return matcher.matches() ? Long.parseLong(matcher.group(1)) : null;
    }

    /**
     * Retourne un Qcm s'il existe
     *
     * @param url Lien
     * @return Qcm|null
     */
    private static Qcm getQCMifExist(String url) {
        Long idQcm = QcmController.getIdQCM(url);
        return idQcm != null ? QcmDAO.getById(idQcm) : null;
    }

    private static Pair<String, Long> getMatchAndId(String url) {
        Matcher idMatch = Regex.QCM_ID.matcher(url);
        boolean idTrue = idMatch.matches();
        Matcher editMatch = Regex.QCM_EDIT.matcher(url);
        boolean editTrue = editMatch.matches();
        if (idTrue || editTrue) {
            long idQcm = 0;
            try {
                if (idTrue) {
                        idQcm = Long.parseLong(idMatch.group(1));
                    return Pair.of("id", idQcm);
                } else {
                    idQcm = Long.parseLong(editMatch.group(1));
                    return Pair.of("edit", idQcm);
                }
            } catch (Exception e) {
                LOGGER.throwing(QcmController.class.getName(),"getMatchAndId",e);
                return Pair.of("",0L);
            }
        } else {
            return Pair.of("",0L);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Qcm qcm = null;
        Pair<String, Long> pair = getMatchAndId(Router.getUrl(req));
        String res = pair.getLeft();
        Long id = pair.getRight();
        if (res.equals("id") || res.equals("edit")) {

            qcm = QcmDAO.getById(id);

            if (qcm != null) {

                QcmDTO qcmDTO = BuilderDTO.buildDTO(qcm, QcmDTO.class);
                req.setAttribute("dto", qcmDTO);

                if (res.equals("id")) {
                    req.setAttribute("view", DISPLAY);
                } else {
                    req.setAttribute("qcmAtt", qcmDTO.getJsonStructure());
                    req.setAttribute("view", "qcm-maker");
                }
            } else {
                req.setAttribute("view", "qcm-error");
            }
        } else {
            req.setAttribute("view", "qcm-maker");
        }
    }

    private static void doAnswer(HttpServletRequest request) {
        Qcm qcm = QcmController.getQCMifExist(Router.getUrl(request));

        if (qcm != null) {
            for (Question question : qcm.getQuestionList()) {
                for (Choice choice : question.getChoiceList()) {

                    String id = String.valueOf(choice.getId());

                    if (request.getParameter(id) != null) {
                        choice.incrementCounter();
                        ChoiceDAO.createOrUpdate(choice);
                    }
                }
            }
            request.setAttribute("view", "answer-ok");
        }
    }

    private static void addUserQcm(HttpServletRequest request, Long id) {
        // If User is correctly logged in then create tuple in association table
        String username = JwtManager.getTokenSubject(request.getSession());
        if (username != null && UsersDAO.getByName(username) != null &&
                !UsersQcmDAO.saveTuple(username, id)) {
            LOGGER.warning("wrong UsersQcmDAO saveTuple");
        }
    }

    private static Pair<Qcm, QcmDTO> getQcmAndDTOPair(HttpServletRequest request) {
        QcmDTO qcmDTO = new QcmDTO();

        try {
            BuilderStrategy.getStrategy(request.getContentType()).parseRequest(
                    request.getReader().lines().collect(Collectors.joining()),
                    qcmDTO);

        }
        catch(Exception e) {
            LOGGER.throwing(QcmController.class.getName(),"getQcmAndDTOPair",e);
        }

        Qcm qcm = qcmDTO.build();
        QcmDAO.createOrUpdate(qcm);
        qcmDTO = BuilderDTO.buildDTO(qcm, QcmDTO.class);

        return Pair.of(qcm, qcmDTO);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (Regex.QCM_ID.matcher(Router.getUrl(request)).matches()) {
            doAnswer(request);
        } else {

            Pair<Qcm, QcmDTO> pair = getQcmAndDTOPair(request);

            addUserQcm(request, pair.getLeft().getId());

            request.setAttribute("dto", pair.getRight());
            request.setAttribute("view", DISPLAY);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (Regex.QCM_ID.matcher(Router.getUrl(req)).matches()) {

            QcmDTO qcmDTO = getQcmAndDTOPair(req).getRight();

            req.setAttribute("dto", qcmDTO);
            req.setAttribute("view", DISPLAY);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Qcm qcm = getQCMifExist(Router.getUrl(req));
        if (qcm != null) {
            QcmDAO.delete(qcm);
        }
    }
}
