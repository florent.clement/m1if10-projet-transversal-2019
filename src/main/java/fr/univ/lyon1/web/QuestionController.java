package fr.univ.lyon1.web;

import fr.univ.lyon1.dao.DAOContext;
import fr.univ.lyon1.dao.QuestionDAO;
import fr.univ.lyon1.dto.BuilderDTO;
import fr.univ.lyon1.dto.QuestionDTO;
import fr.univ.lyon1.model.Question;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;

@WebServlet(name = "question")
public class QuestionController extends HttpServlet {

    @Override
    public void init() throws ServletException {
        DAOContext.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Question question = QuestionController.getQuestionIfExist(Router.getUrl(req));

        // Vérification que le choix existe bien
        if (question != null) {

            QuestionDTO questionDTO = BuilderDTO.buildDTO(question, QuestionDTO.class);

            req.setAttribute("dto", questionDTO);
            req.setAttribute("view", "question");

        }
    }

    private static Question getQuestionIfExist(String url) {
        Long idQuestion = QuestionController.getIdQuestion(url);
        return idQuestion != null ? QuestionDAO.getById(idQuestion) : null;
    }

    private static Long getIdQuestion(String url) {
        Matcher matcher = Regex.QUESTIONS_ID.matcher(url);
        return matcher.matches() ? Long.parseLong(matcher.group(2)) : null;
    }
}
