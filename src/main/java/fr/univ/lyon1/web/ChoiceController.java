package fr.univ.lyon1.web;

import fr.univ.lyon1.dao.ChoiceDAO;
import fr.univ.lyon1.dao.DAOContext;
import fr.univ.lyon1.dto.BuilderDTO;
import fr.univ.lyon1.dto.ChoiceDTO;
import fr.univ.lyon1.model.Choice;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;

@WebServlet(name = "choice")
public class ChoiceController extends HttpServlet {

    @Override
    public void init() throws ServletException {
        DAOContext.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        Choice choice = ChoiceController.getChoiceIfExist(Router.getUrl(req));

        System.out.println(Router.getUrl(req));
        /*
         * Vérification que le choix existe bien
         */
        if (choice != null) {

            ChoiceDTO choiceDTO = BuilderDTO.buildDTO(choice, ChoiceDTO.class);

            req.setAttribute("dto", choiceDTO);
            req.setAttribute("view", "choix");
        }
    }

    private static Choice getChoiceIfExist(String url) {
        Long idQcm = ChoiceController.getIdChoice(url);
        return idQcm != null ? ChoiceDAO.getById(idQcm) : null;
    }

    private static Long getIdChoice(String url) {
        Matcher matcher = Regex.CHOICE_ID.matcher(url);
        return matcher.matches() ? Long.parseLong(matcher.group(3)) : null;
    }

}