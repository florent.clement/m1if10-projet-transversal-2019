package fr.univ.lyon1.manager;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.logging.Logger;

import static com.auth0.jwt.impl.PublicClaims.ALGORITHM;
import static com.auth0.jwt.impl.PublicClaims.ISSUER;

public class JwtManager {

    private static final Logger LOGGER = Logger.getLogger(JwtManager.class.getName());

    private String token;
    private JWTCreator.Builder builderToken;

    public JwtManager(String user) {
        this.builderToken = this.buildToken(user);
        this.token = builderToken.sign(Algorithm.HMAC256(ALGORITHM));
    }

    public void createOrUpdateSessionToken(HttpSession httpSession) {
        httpSession.setAttribute("token",this.token);
    }

    private JWTCreator.Builder buildToken(String user) {
        JWTCreator.Builder jwtBuilder = JWT.create();
        jwtBuilder.withIssuer(ISSUER)
                .withSubject(user)
                .withExpiresAt(Date.from( LocalDateTime.now().plusMinutes(30)
                        .atZone( ZoneId.systemDefault()).toInstant()));

        return jwtBuilder;
    }

    public String getToken() {
        return token;
    }

    public static String getTokenSubject(HttpSession httpSession) {
        String token = (String)httpSession.getAttribute("token");
        if(token != null) {
            try {
                DecodedJWT djwt = JWT.decode(token);
                Date now = java.util.Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
                if (now.getTime() < djwt.getExpiresAt().getTime()) {
                    return djwt.getSubject();
                } else {
                    return null;
                }
            } catch (JWTDecodeException e) {
                LOGGER.throwing(JwtManager.class.getName(), "getTokenSubject", e);
                return null;
            }
        }
        return null;
    }
}
