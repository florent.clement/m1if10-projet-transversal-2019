package fr.univ.lyon1.dto;

import fr.univ.lyon1.model.Users;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.json.*;
import java.util.Map;

public class UsersDTO extends DTO {

    private String name;
    private int password;
    private final String[] atts = ("name;password").split(";");

    @Override
    public JsonObject getJsonStructure() {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
        jsonObjectBuilder.add(atts[0],this.name);
        jsonObjectBuilder.add(atts[1],this.password);
        return jsonObjectBuilder.build();
    }

    @Override
    public UsersDTO parseJson(JsonStructure jsonStructure) {
        JsonObject jsonObject = (JsonObject)jsonStructure;

        this.name = jsonObject.getString(atts[0]);
        this.password = jsonObject.getInt(atts[1]);

        return this;
    }

    @Override
    public Element getElementXML(Document document) {
        Element root = document.createElement("user");

        //id
        Element elementName = document.createElement(atts[0]);
        elementName.appendChild(document.createTextNode(this.name));
        root.appendChild(elementName);

        //counter
        Element elementPassword = document.createElement(atts[1]);
        elementPassword.appendChild(document.createTextNode(String.valueOf(this.password)));
        root.appendChild(elementPassword);

        return root;
    }

    @Override
    public UsersDTO parseXML(Element element) {

        //content
        this.name = element.getElementsByTagName(atts[0]).item(0).getTextContent();

        //counter
        this.password = Integer.parseInt(
                element.getElementsByTagName(atts[1]).item(0).getTextContent());


        return this;
    }

    @Override
    public UsersDTO parseFormData(Map<String, String> formDataContent) {
        return null;
    }

    @Override
    public Users build() {
        return this.build(Users.class);
    }
}
