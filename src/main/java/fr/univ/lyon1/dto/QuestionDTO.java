package fr.univ.lyon1.dto;

import fr.univ.lyon1.dao.QcmDAO;
import fr.univ.lyon1.model.Choice;
import fr.univ.lyon1.model.Qcm;
import fr.univ.lyon1.model.Question;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.json.*;
import java.util.*;

public class QuestionDTO extends DTO {

    private Long id;
    private String content;
    private Qcm qcm;
    private List<Choice> choiceList = new ArrayList<>();
    private String[] atts = ("id;idQcm;content;choices").split(";");

    @Override
    public JsonObject getJsonStructure() {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();

        if(this.id != null ) {
            jsonObjectBuilder.add(atts[0], this.id);
        }
        jsonObjectBuilder.add(atts[1],this.qcm.getId());
        jsonObjectBuilder.add(atts[2],this.content);

        // list choix
        JsonArrayBuilder jsonArrayBuilderChoix = Json.createArrayBuilder();
        for(Choice choice : choiceList) {
            ChoiceDTO choiceDTO = BuilderDTO.buildDTO(choice,ChoiceDTO.class);
            jsonArrayBuilderChoix.add(choiceDTO.getJsonStructure());
        }
        jsonObjectBuilder.add(atts[3],jsonArrayBuilderChoix.build());

        return jsonObjectBuilder.build();
    }

    @Override
    public QuestionDTO parseJson(JsonStructure jsonStructure) {
        JsonObject jsonObject = (JsonObject)jsonStructure;

        if(jsonObject.containsKey(atts[0])) {
            this.id = Long.parseLong(String.valueOf(jsonObject.getInt(atts[0])));
        }
        this.content = jsonObject.getString(atts[2]);

        if(jsonObject.containsKey(atts[1])) {
            Long idQcm = Long.parseLong(String.valueOf(jsonObject.getInt(atts[1])));
            this.qcm = QcmDAO.getById(idQcm);
        }

        if(jsonObject.containsKey(atts[3])) {
            JsonArray jsonArrayChoices = jsonObject.getJsonArray(atts[3]);
            for(int i = 0; i < jsonArrayChoices.size(); i++) {
                ChoiceDTO choiceDTO = new ChoiceDTO();
                choiceDTO.parseJson(jsonArrayChoices.getJsonObject(i));
                choiceList.add(choiceDTO.build());
            }
        }

        return this;
    }

    @Override
    public Element getElementXML(Document document) {
        Element root = document.createElement("question");

        //id
        Element elementId = document.createElement(atts[0]);
        elementId.appendChild(document.createTextNode(String.valueOf(this.id)));
        root.appendChild(elementId);

        //idQuestion
        Element idQuestion = document.createElement(atts[1]);
        idQuestion.appendChild(document.createTextNode(String.valueOf(this.qcm.getId())));
        root.appendChild(idQuestion);

        //content
        Element elementContent = document.createElement(atts[2]);
        elementContent.appendChild(document.createTextNode(this.content));
        root.appendChild(elementContent);

        Element choices = document.createElement(atts[3]);
        for(Choice choice : choiceList) {
            ChoiceDTO choiceDTO = BuilderDTO.buildDTO(choice,ChoiceDTO.class);
            choices.appendChild(choiceDTO.getElementXML(document));
        }
        root.appendChild(choices);

        return root;
    }

    @Override
    public QuestionDTO parseXML(Element element) {
        //id
        this.id = Long.parseLong(
                element.getElementsByTagName(atts[0]).item(0).getTextContent());

        //content
        this.content = element.getElementsByTagName(atts[2]).item(0).getTextContent();


        //idQcm
        Long idQcm = Long.parseLong(
                element.getElementsByTagName(atts[1]).item(0).getTextContent());
        this.qcm = QcmDAO.getById(idQcm);

        return this;
    }

    @Override
    public QuestionDTO parseFormData(Map<String, String> formDataContent) {

        if(formDataContent.containsKey(atts[0])) {
            this.id = Long.parseLong(String.valueOf(formDataContent.get(atts[0])));
        }
        this.content = formDataContent.get(atts[2]);

        if(formDataContent.containsKey(atts[1])) {
            Long idQcm = Long.parseLong(String.valueOf(formDataContent.get(atts[1])));
            this.qcm = QcmDAO.getById(idQcm);
        }

        return this;
    }

    @Override
    public Question build() {
        Question question = this.build(Question.class);
        for(Choice choice : question.getChoiceList()) {
            choice.setQuestion(question);
        }
        return question;
    }
}
