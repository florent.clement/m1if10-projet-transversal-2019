package fr.univ.lyon1.dto;

import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.modelmapper.convention.NamingConventions;

public class BuilderDTO {

    private BuilderDTO() {

    }

    private static ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setFieldMatchingEnabled(true)
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE)
                .setSourceNamingConvention(NamingConventions.JAVABEANS_MUTATOR);
        return modelMapper;
    }

     public static <T,E>  E buildDTO(T object, Class<? extends E> formatDTO) {
         return modelMapper().map(object,formatDTO);
     }
     public static <T> T buildEntity(T object, Class<? extends T> format) {
         return modelMapper().map(object,format);
     }
}
