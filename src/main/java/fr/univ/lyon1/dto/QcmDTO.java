package fr.univ.lyon1.dto;

import fr.univ.lyon1.model.Qcm;
import fr.univ.lyon1.model.Question;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.json.*;
import java.util.ArrayList;
import java.util.Map;

public class QcmDTO extends DTO {

    private Long id;
    private String titre;
    private ArrayList<Question> questionList = new ArrayList<>();
    private String[] atts = ("id;titre;questions").split(";");

    @Override
    public JsonObject getJsonStructure() {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
        jsonObjectBuilder.add(atts[0],this.id);
        jsonObjectBuilder.add(atts[1], this.titre);

        // list choix
        JsonArrayBuilder jsonArrayBuilderChoix = Json.createArrayBuilder();
        for(Question question : questionList) {
            QuestionDTO questionDTO = BuilderDTO.buildDTO(question,QuestionDTO.class);
            jsonArrayBuilderChoix.add(questionDTO.getJsonStructure());
        }
        jsonObjectBuilder.add(atts[2],jsonArrayBuilderChoix.build());

        return jsonObjectBuilder.build();
    }

    @Override
    public QcmDTO parseJson(JsonStructure jsonStructure) {
        JsonObject jsonObject = (JsonObject)jsonStructure;

        if(jsonObject.containsKey(atts[0])) {
            this.id = Long.parseLong(String.valueOf(jsonObject.getInt(atts[0])));
        }

        if(jsonObject.containsKey(atts[1])) {
            this.titre = jsonObject.getString(atts[1]);
        }

        if(jsonObject.containsKey(atts[2])) {
            JsonArray jsonArrayQuestions = jsonObject.getJsonArray(atts[2]);
            for(int i = 0; i < jsonArrayQuestions.size(); i++) {
                QuestionDTO questionDTO = new QuestionDTO();
                questionDTO.parseJson(jsonArrayQuestions.getJsonObject(i));
                questionList.add(questionDTO.build());
            }
        }
        return this;
    }

    @Override
    public Element getElementXML(Document document) {
        Element root = document.createElement("qcm");

        //id
        Element elementId = document.createElement(atts[0]);
        elementId.appendChild(document.createTextNode(String.valueOf(this.id)));
        root.appendChild(elementId);

        //titre
        Element elementTitre = document.createElement(atts[1]);
        elementTitre.appendChild(document.createTextNode(this.titre));
        root.appendChild(elementTitre);

        Element questions = document.createElement(atts[2]);
        for(Question question : questionList) {
            QuestionDTO questionDTO = BuilderDTO.buildDTO(question,QuestionDTO.class);
            questions.appendChild(questionDTO.getElementXML(document));
        }
        root.appendChild(questions);

        return root;
    }

    @Override
    public QcmDTO parseXML(Element element) {
        //id
        this.id = Long.parseLong(
                element.getElementsByTagName(atts[0]).item(0).getTextContent());

        return this;
    }

    @Override
    public QcmDTO parseFormData(Map<String, String> formDataContent) {

        if(formDataContent.containsKey(atts[0])) {
            this.id = Long.parseLong(String.valueOf(formDataContent.get(atts[0])));
        }
        this.titre = formDataContent.get(atts[1]);

        return this;
    }

    @Override
    public Qcm build() {

        Qcm qcm = this.build(Qcm.class);
        for(Question question : qcm.getQuestionList()) {
            question.setQcm(qcm);
        }
        return qcm;
    }
}
