package fr.univ.lyon1.dto;

import fr.univ.lyon1.dao.QuestionDAO;
import fr.univ.lyon1.model.Choice;
import fr.univ.lyon1.model.Question;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonStructure;
import java.util.Map;

public class ChoiceDTO extends DTO {

    private Long id;
    private Question question;
    private String content;
    private int counter;
    private String[] atts = ("id;idQuestion;content;counter").split(";");

    @Override
    public JsonObject getJsonStructure() {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
        if(this.id != null ) {
            jsonObjectBuilder.add(atts[0], this.id);
        }
        if(this.question != null) {
            if (this.question.getId() != null) {
                jsonObjectBuilder.add(atts[1], this.question.getId());
            }
        }
        jsonObjectBuilder.add(atts[2],this.content);
        jsonObjectBuilder.add(atts[3],this.counter);
        return jsonObjectBuilder.build();
    }

    @Override
    public ChoiceDTO parseJson(JsonStructure jsonStructure) {
        JsonObject jsonObject = (JsonObject)jsonStructure;

        if(jsonObject.containsKey(atts[0])) {
            this.id = Long.parseLong(String.valueOf(jsonObject.getInt(atts[0])));
        }

        this.content = jsonObject.getString(atts[2]);
        this.counter = jsonObject.getInt(atts[3]);

        if(jsonObject.containsKey(atts[1])) {
            Long idQuestion = Long.parseLong(String.valueOf(jsonObject.getInt(atts[1])));
            this.question = QuestionDAO.getById(idQuestion);
        }

        return this;
    }

    @Override
    public Element getElementXML(Document document) {
        Element root = document.createElement("choice");

        //id
        Element elementId = document.createElement(atts[0]);
        elementId.appendChild(document.createTextNode(String.valueOf(this.id)));
        root.appendChild(elementId);

        //idQuestion
        Element idQuestion = document.createElement(atts[1]);
        idQuestion.appendChild(document.createTextNode(String.valueOf(this.question.getId())));
        root.appendChild(idQuestion);

        //content
        Element elementContent = document.createElement(atts[2]);
        elementContent.appendChild(document.createTextNode(this.content));
        root.appendChild(elementContent);

        //counter
        Element elementCounter = document.createElement(atts[3]);
        elementCounter.appendChild(document.createTextNode(String.valueOf(this.counter)));
        root.appendChild(elementCounter);

        return root;
    }

    @Override
    public ChoiceDTO parseXML(Element element) {

        //id
        this.id = Long.parseLong(
                element.getElementsByTagName(atts[0]).item(0).getTextContent());

        //content
        this.content = element.getElementsByTagName(atts[2]).item(0).getTextContent();

        //counter
        this.counter = Integer.parseInt(
                element.getElementsByTagName(atts[3]).item(0).getTextContent());


        //idQuestion
        Long idQuestion = Long.parseLong(
                element.getElementsByTagName(atts[1]).item(0).getTextContent());
        this.question = QuestionDAO.getById(idQuestion);


        return this;
    }

    @Override
    public ChoiceDTO parseFormData(Map<String, String> formDataContent) {

        if(formDataContent.containsKey(atts[0])) {
            this.id = Long.parseLong(String.valueOf(formDataContent.get(atts[0])));
        }

        this.content = formDataContent.get(atts[2]);
        this.counter = Integer.parseInt(formDataContent.get(atts[3]));

        if(formDataContent.containsKey(atts[1])) {
            Long idQuestion = Long.parseLong(String.valueOf(formDataContent.get(atts[1])));
            this.question = QuestionDAO.getById(idQuestion);
        }

        return this;
    }

    @Override
    public Choice build() {
        return this.build(Choice.class);
    }
}
