package fr.univ.lyon1.dto;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.json.JsonStructure;
import javax.servlet.http.HttpServletRequest;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.Map;

public abstract class DTO {

    public abstract JsonStructure getJsonStructure();
    public abstract DTO parseJson(JsonStructure jsonStructure);

    public abstract Element getElementXML(Document document);
    public Element getElementXML() {

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            return this.getElementXML(document);
        } catch (ParserConfigurationException e) {
            return null;
        }
    }

    public static String getBaseUrl(HttpServletRequest req) {
        StringBuffer url = req.getRequestURL();
        String uri = req.getRequestURI();
        String ctx = req.getContextPath();
        return url.substring(0, url.length() - uri.length() + ctx.length()) + "/";
    }


    public abstract DTO parseXML(Element element);

    public abstract DTO parseFormData(Map<String,String> formDataContent);

    protected  <T> T build(Class<? extends T> format) {
        return (T) BuilderDTO.buildEntity(this,format);
    }

    public abstract Object build();
}
