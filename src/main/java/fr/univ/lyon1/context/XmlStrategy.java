package fr.univ.lyon1.context;

import fr.univ.lyon1.dto.DTO;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.logging.Logger;

public class XmlStrategy implements ResourcesStrategy {

    private static final Logger LOGGER = Logger.getLogger(XmlStrategy.class.getName());

    private String convertDocumentIntoString(Document document) throws TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        tf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(document), new StreamResult(writer));
        return writer.getBuffer().toString().replaceAll("\n|\r", "");
    }

    @Override
    public String getContentPage(DTO dto) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            document.appendChild(dto.getElementXML(document));
            return this.convertDocumentIntoString(document);
        } catch (ParserConfigurationException | TransformerException e) {
            LOGGER.throwing(XmlStrategy.class.getName(), "getContentPage", e);
            return null;
        }
    }

    @Override
    public void parseRequest(String content, DTO dto) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            InputStream inputStream = new ByteArrayInputStream(content.getBytes());
            Document document = documentBuilder.parse(inputStream);
            dto.parseXML(document.getDocumentElement());
        } catch (ParserConfigurationException | SAXException | IOException e) {
            LOGGER.throwing(XmlStrategy.class.getName(), "parseRequest", e);
        }
    }
}
