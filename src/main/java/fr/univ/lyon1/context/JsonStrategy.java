package fr.univ.lyon1.context;

import fr.univ.lyon1.dto.DTO;

import javax.json.Json;
import javax.json.JsonReader;
import java.io.StringReader;

public class JsonStrategy implements ResourcesStrategy {

    @Override
    public String getContentPage(DTO dto) {
        return dto.getJsonStructure().toString();
    }

    @Override
    public void parseRequest(String content, DTO dto) {
        JsonReader jsonReader = Json.createReader(new StringReader(content));
        dto.parseJson(jsonReader.read());
        jsonReader.close();
    }
}
