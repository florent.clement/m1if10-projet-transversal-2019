package fr.univ.lyon1.context;

import fr.univ.lyon1.dto.DTO;

public interface ResourcesStrategy {

    String getContentPage(DTO dto);

    void parseRequest(String content, DTO dto);
}
