package fr.univ.lyon1.context;

public class BuilderStrategy {

    private BuilderStrategy() {

    }

    public static ResourcesStrategy getStrategy(String contentType) {

        if(contentType.startsWith("application/xml")) {
            return new XmlStrategy();
        } else if(contentType.startsWith("application/json") ) {
            return new JsonStrategy();
        } else if(contentType.startsWith("application/x-www-form-urlencoded")) {
            return new FormDataStrategy();
        } else {
            return null;
        }
    }
}
