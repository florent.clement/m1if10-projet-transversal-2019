package fr.univ.lyon1.context;

import fr.univ.lyon1.dto.DTO;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class FormDataStrategy implements ResourcesStrategy {

    private static final Logger LOGGER = Logger.getLogger(FormDataStrategy.class.getName());

    /**
     * Source : https://stackoverflow.com/questions/13592236/parse-a-uri-string-into-name-value-collection
     *
     * @param url
     * @return
     */
    private static Map<String, String> splitQuery(String url) {
        Map<String, String> queryPairs = new HashMap<>();
        try {
            String[] pairs = url.split("&");
            for (String pair : pairs) {
                int idx = pair.indexOf('=');
                queryPairs.put(URLDecoder.decode(pair.substring(0, idx), String.valueOf(StandardCharsets.UTF_8)),
                        URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
            }
        } catch (UnsupportedEncodingException e) {
            LOGGER.throwing(FormDataStrategy.class.getName(),"splitQuery", e);
        }
        return queryPairs;
    }

    /**
     *  Ne prend pas en charge la conversion au format www encode form.
     * @param dto
     * @return null
     */
    // Deprecated
    @Override
    public String getContentPage(DTO dto) {
        return null;
    }

    @Override
    public void parseRequest(String content, DTO dto) {
        dto.parseFormData(splitQuery(content));
    }
}
