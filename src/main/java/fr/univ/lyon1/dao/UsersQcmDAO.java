package fr.univ.lyon1.dao;

import fr.univ.lyon1.model.Qcm;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class UsersQcmDAO {

    private static final Logger LOGGER = Logger.getLogger(UsersQcmDAO.class.getName());

    private UsersQcmDAO() {

    }

    public static boolean saveTuple(String username, long qcmId) {

        if (username != null) {

            String qry = "INSERT INTO users_qcm SET users_qcm.name_users = ?0, users_qcm.id_qcm = ?1";
            EntityManager em = DAOContext.getEntityManager();
            EntityTransaction et = null;
            Query query = null;
            int res;

            try {
                et = em.getTransaction();

                et.begin();

                query = em.createNativeQuery(qry);
                query.setParameter(0, username);
                query.setParameter(1, qcmId);
                res = query.executeUpdate();

                et.commit();

                return res == 1;

            } catch (Exception e) {
                if (et != null) {
                    et.rollback();
                }
                LOGGER.warning(e.getMessage());
                return false;
            }

        } else {
            LOGGER.warning("NULL username in saveTuple");
            return false;
        }
    }

    public static List<Qcm> getQcmIdList(String username) {

        if (username != null) {

            EntityManager em = DAOContext.getEntityManager();

            try {
                TypedQuery<Qcm> qcmTypedQuery = em.createNamedQuery("qcm.getQcmIdList",Qcm.class);
                qcmTypedQuery.setParameter(0, username);

                return qcmTypedQuery.getResultList();

            } catch (Exception e) {
                LOGGER.warning(e.getMessage());
                return new ArrayList<>();
            }

        } else {
            LOGGER.warning("NULL username in getQcmIdList");
            return new ArrayList<>();
        }
    }

    public static String getUserName(long qcmId) {

        EntityManager em = DAOContext.getEntityManager();
        String qry = "SELECT DISTINCT users_qcm.name_users FROM users_qcm WHERE users_qcm.id_qcm = ?0";
        Query query = null;
        String res = null;

        try {

            query = em.createNativeQuery(qry);
            query.setParameter(0, qcmId);
            res = (String) query.getSingleResult();

            return res;

        } catch (Exception e) {
            LOGGER.warning(e.getMessage());
            return null;
        }
    }

}