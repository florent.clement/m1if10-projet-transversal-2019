package fr.univ.lyon1.dao;

import fr.univ.lyon1.model.Users;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.logging.Logger;

public class UsersDAO {

    private static final Logger LOGGER = Logger.getLogger(UsersDAO.class.getName());

    private UsersDAO() {

    }

    /**
     * Create user entry in database.
     * @param user model.
     * @return false if the user in database already exists,
     * tre if it was correctly created.
     */
    public static boolean create(Users user) {

        EntityManager em = DAOContext.getEntityManager();
        EntityTransaction et = null;
        // If no user with same name exists
        if (getByName(user.getName()) == null) {
            try {
                et = em.getTransaction();

                et.begin();

                em.persist(user);

                et.commit();

                return true;

            } catch (Exception e) {
                if (et != null) {
                    et.rollback();
                }
                LOGGER.throwing(UsersDAO.class.getName(),"create",e);
                return false;
            }
        } else return false;
    }

    public static void delete(Users user) {

        EntityManager em = DAOContext.getEntityManager();
        EntityTransaction et = null;

        try {
            et = em.getTransaction();

            et.begin();
            em.remove(user);
            et.commit();
        } catch (Exception e) {
            if (et != null) {
                et.rollback();
            }
            LOGGER.throwing(UsersDAO.class.getName(),"delete",e);
        }
    }

    public static Users getByName(String name) {

        EntityManager em = DAOContext.getEntityManager();

        return em.find(Users.class, name);

    }
}
