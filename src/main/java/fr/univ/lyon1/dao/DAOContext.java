package fr.univ.lyon1.dao;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public abstract class DAOContext {

    private DAOContext() {

    }

    private static EntityManager entityManager;

    public static void init() {
        entityManager =
                Persistence.createEntityManagerFactory("bdd")
                        .createEntityManager();

    }

    public static EntityManager getEntityManager() {
        return entityManager;
    }
}
