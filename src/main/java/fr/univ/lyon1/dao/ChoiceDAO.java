package fr.univ.lyon1.dao;

import fr.univ.lyon1.model.Choice;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.logging.Logger;

public class ChoiceDAO {

    private static final Logger LOGGER = Logger.getLogger(ChoiceDAO.class.getName());

    private ChoiceDAO() {

    }

    public static void createOrUpdate(Choice choice) {

        EntityManager em = DAOContext.getEntityManager();
        EntityTransaction et = null;

        try {
            et = em.getTransaction();

            et.begin();

            em.persist(choice);

            et.commit();

        } catch (Exception e) {
            if (et != null) {
                et.rollback();
            }
            LOGGER.throwing(ChoiceDAO.class.getName(),"Create or Update", e);
        }
    }

    public static void updateCounter(Choice choice, int counter) {

        EntityManager em = DAOContext.getEntityManager();
        EntityTransaction et = null;

        try {
            et = em.getTransaction();

            et.begin();
            choice.setCounter(counter);
            et.commit();
        } catch (Exception e) {
            if (et != null) {
                et.rollback();
            }
            LOGGER.throwing(ChoiceDAO.class.getName(),"updateCounter", e);
        }
    }

    public static void delete(Choice choice) {

        EntityManager em = DAOContext.getEntityManager();
        EntityTransaction et = null;

        try {
            et = em.getTransaction();

            et.begin();
            em.remove(choice);
            et.commit();
        } catch (Exception e) {
            if (et != null) {
                et.rollback();
            }
            LOGGER.throwing(ChoiceDAO.class.getName(),"delete", e);
        }
    }

    public static Choice getById(Long id) {

        EntityManager em = DAOContext.getEntityManager();

        return em.find(Choice.class, id);

    }
}
