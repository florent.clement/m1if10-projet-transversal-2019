package fr.univ.lyon1.dao;

import fr.univ.lyon1.model.Question;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.logging.Logger;

public class QuestionDAO {

    private static final Logger LOGGER = Logger.getLogger(QuestionDAO.class.getName());

    private QuestionDAO() {

    }

    public static void createOrUpdate(Question question) {

        EntityManager em = DAOContext.getEntityManager();
        EntityTransaction et = null;

        try {
            et = em.getTransaction();

            et.begin();

            em.persist(question);

            et.commit();

        } catch (Exception e) {
            if (et != null) {
                et.rollback();
            }
            LOGGER.throwing(Question.class.getName(),"createOrUpdate",e);
        }
    }

    public static void delete(Question question) {

        EntityManager em = DAOContext.getEntityManager();
        EntityTransaction et = null;

        try {
            et = em.getTransaction();

            et.begin();
            em.remove(question);
            et.commit();
        } catch (Exception e) {
            if (et != null) {
                et.rollback();
            }
            LOGGER.throwing(Question.class.getName(),"delete",e);
        }
    }

    public static Question getById(Long id) {

        EntityManager em = DAOContext.getEntityManager();

        return em.find(Question.class, id);
    }
}
