package fr.univ.lyon1.dao;

import fr.univ.lyon1.model.Qcm;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.LockModeType;
import java.util.logging.Logger;

public class QcmDAO {

    private static final Logger LOGGER = Logger.getLogger(QcmDAO.class.getName());

    private QcmDAO() {

    }

    public static void createOrUpdate(Qcm qcm) {

        EntityManager em = DAOContext.getEntityManager();
        EntityTransaction et = null;

        try {
            et = em.getTransaction();

            et.begin();

            if(qcm.getId() != null) {
                qcm = em.merge(qcm);
            } else {
                em.persist(qcm);
            }

            et.commit();

            em.refresh(qcm);

        } catch (Exception e) {
            if (et != null && et.isActive()) {
                et.rollback();
            }
            LOGGER.throwing(QcmDAO.class.getName(),"createOrUpdate",e);
        }
    }

    public static void delete(Qcm qcm) {

        EntityManager em = DAOContext.getEntityManager();
        EntityTransaction et = null;

        try {
            et = em.getTransaction();

            et.begin();
            em.remove(qcm);
            et.commit();

        } catch (Exception e) {
            if (et != null && et.isActive()) {
                et.rollback();
            }
            LOGGER.throwing(QcmDAO.class.getName(),"delete",e);
        }
    }

    public static Qcm getById(Long id) {

        EntityManager em = DAOContext.getEntityManager();
        EntityTransaction et = null;

        try {

            et = em.getTransaction();

            Qcm qcm = em.find(Qcm.class,id);

            if(qcm != null) {

                if (qcm.getQuestionList() != null) {
                    qcm.getQuestionList().clear();
                }

                et.begin();
                em.refresh(qcm, LockModeType.READ);
                em.refresh(qcm, LockModeType.NONE);
                et.commit();

                return qcm;

            } else {
                return null;
            }
        } catch(Exception e) {
            if (et != null && et.isActive()) {
                et.rollback();
            }
            LOGGER.throwing(QcmDAO.class.getName(),"getById",e);
        }

        return null;
    }
}
