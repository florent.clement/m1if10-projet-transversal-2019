var QcmDeleter = function(element, context, id) {
    this.element = element;
    this.context = context;
    this.id = id;
};

QcmDeleter.prototype.show = function() {
  $("#qcm-delete-message").modal();
  $(".modal-backdrop").hide();
  var button = document.getElementById("delete-button");
  var parent = this.element.parentNode.parentNode.parentNode;
  button.onclick = function() {
      $.ajax({
                 url: context + '/qcm/' + id,
                 type: 'delete'
      });
      parent.parentNode.removeChild(parent);
      $("#qcm-delete-message").hide();
  };
};

function createDelete(element, context, id) {
    var qcmDeleter = new QcmDeleter(element, context, id);
    qcmDeleter.show();
}