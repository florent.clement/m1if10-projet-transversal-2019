var QcmMaker = function(context, qrBase, id) {
    this.titre = "";
    this.listChart = [];
    this.context = context;
    this.qrBase = qrBase;
    if(!isNaN(id)) {
        this.id = id;
        this.load = false;
        this.getQCM();
        this.initQRCode();
    } else {
        this.id = null;
        var questionsDiv = $(".questions")[0];
        questionsDiv.insertBefore(this.createQuestion(),
            $(".questions > button")[0]);
        this.defineAddChoice();
        this.defineAddQuestion();
        this.defineRemoveChoice();
        this.defineRemoveQuestion();
    }

};

QcmMaker.prototype.showModal = function() {
    document.getElementById("qcm-message-link").innerHTML =
        "<a href=" + this.qrBase + this.context + "/qcm/" + this.id + 'target="_blank">'
        +  this.qrBase + this.context + "/qcm/" + this.id + "</a>";
    $("#qcm-message").modal();
    $(".modal-backdrop").hide();
}

QcmMaker.prototype.initQRCode = function() {
    $(document.getElementById("divQrcode")).qrcode(
        this.qrBase + this.context + "/qcm/" + this.id);
};

QcmMaker.prototype.buildListChart = function(data) {
    var section = document.getElementById("qcm-graphs");
    document.getElementById("title-qcm").innerText = this.titre;

    if(data.questions.length !== this.listChart.length) {

        if(data.questions.length > this.listChart.length) {
            for(var i = this.listChart.length; i < data.questions.length; i++) {
                section.appendChild(document.createElement("div"));
                this.listChart.push(new ChartQCM(section.lastChild));
            }
        } else {
            while(data.questions.length !== this.listChart.length) {
                var chart = this.listChart.pop();
                chart.myChart._listeners.resize = null;  // supprime la fonction resize
                chart.myChart.destroy();
                chart.element.remove();
            }
        }

    }
};

QcmMaker.prototype.loadQCM = function(data){
    $("#titleQCM").val(data.titre);
    var questionsDiv = $(".questions")[0];
    for(var i = 0; i < data.questions.length; i++ ) {
        var questionDiv = this.createQuestion(data.questions[i].content);
        for(var j = 0; j < data.questions[i].choices.length; j++ ) {
            questionDiv.appendChild(this.createChoice(
                data.questions[i].choices[j].content)
            );
        }
        questionsDiv.insertBefore(questionDiv,
            $(".questions > button")[0]);
    }
    this.defineAddChoice();
    this.defineAddQuestion();
    this.defineRemoveChoice();
    this.defineRemoveQuestion();
};

QcmMaker.prototype.getQCM = function(){

    var self = this;

    setInterval(function () {
        $.ajax({
            url: self.context + "/qcm/" + self.id,
            dataType: 'json',
            success: function( data ) {
                if(self.load === false) {
                    self.loadQCM(data);
                    self.load = true;
                }
                self.buildListChart(data);
                for(var i = 0; i < data.questions.length; i++ ) {
                    self.listChart[i].updateChart(data.questions[i]);
                }
            }
        });
    },2000);

};



QcmMaker.prototype.createRemoveButtonFormChoice = function() {
    var span = document.createElement("span");
    $(span).addClass("input-group-btn");

    var button = document.createElement("button");
    button.type = "button";
    button.textContent = "-";
    button.style.zIndex = "1";
    $(button).addClass("btn btn-danger");
    span.appendChild(button);
    return span;
};

QcmMaker.prototype.createRemoveButtonFormQuestion = function() {
    var span = document.createElement("span");
    $(span).addClass("input-group-btn");

    var button = document.createElement("button");
    button.type = "button";
    button.textContent = "- Question";
    button.style.zIndex = "1";
    $(button).addClass("btn btn-danger");
    span.appendChild(button);
    return span;
};


QcmMaker.prototype.createInputFormChoice = function(content) {
    var input = document.createElement("input");
    $(input).addClass("form-control");
    input.placeholder = "Entrer une réponse ici";
    if (content !== null && content !== undefined) {
        input.value = content;
    }
    input.type = "text";
    return input;
};


QcmMaker.prototype.createNodeFormChoice = function(content) {
    var element = document.createElement("div");
    $(element).addClass("input-group");
    element.appendChild(this.createInputFormChoice(content));
    element.appendChild(this.createRemoveButtonFormChoice());
    return element;
};


//Création d'un choix
QcmMaker.prototype.createChoice = function(content) {

    // création du div du choix
    var element = document.createElement("div");
    $(element).addClass("choice");
    element.appendChild(this.createNodeFormChoice(content));
    return element;
};


// Ajout d'un choix
QcmMaker.prototype.defineAddChoice = function() {
    var self = this;
    $(".choices > button").off('click');
    $(".choices > button").click(function (event) {
        var choices = $(event.target.parentNode).find(".choice");

        if(choices.length < 6) {
            event.target.parentNode.insertBefore(self.createChoice(),
                event.target.parentNode.children[choices.length]);
        }
        self.defineRemoveChoice();

    });
};

QcmMaker.prototype.createInputFormQuestion = function(content) {
    var input = document.createElement("input");
    $(input).addClass("form-control");
    input.placeholder = "Entrer la question ici";
    if (content !== null && content !== undefined) {
        input.value = content;
    }
    input.type = "text";
    return input;
};

QcmMaker.prototype.createLabelFormQuestion = function() {
    var label = document.createElement("label");
    $(label).addClass("bmd-label-static");
    label.textContent = "Question ?";
    return label;
};

QcmMaker.prototype.createNodeFormQuestion = function(content) {
    var element = document.createElement("div");
    $(element).addClass("form-group bmd-form-group");
    element.appendChild(this.createLabelFormQuestion());
    element.appendChild(this.createInputFormQuestion(content));
    element.appendChild(this.createRemoveButtonFormQuestion());
    return element;
};

QcmMaker.prototype.createSectionChoices = function() {
    var section = document.createElement("section");
    $(section).addClass("choices");
    section.appendChild(this.createChoice());

    var button = document.createElement("button");
    button.type = "button";
    $(button).addClass("btn btn-success");
    button.textContent = "+";
    section.appendChild(button);

    return section;
};

//Création d'une question
QcmMaker.prototype.createQuestion = function(content) {

    // création du div de question
    var element = document.createElement("div");
    $(element).addClass("question");
    element.appendChild(this.createNodeFormQuestion(content));
    element.appendChild(document.createElement("br"));
    if(content === null || content === undefined)
        element.appendChild(this.createSectionChoices());
    return element;
};

QcmMaker.prototype.defineAddQuestion = function() {
    var self = this;
    $(".questions > button").off('click');
    $(".questions > button").click(function (event) {
        var questions = $(event.target.parentNode).find(".question");

        if(questions.length < 6) {
            event.target.parentNode.insertBefore(self.createQuestion(),
                $(".questions > button")[0]);

            self.defineAddChoice();
        }
        self.defineRemoveChoice();
        self.defineRemoveQuestion();

    });
};

QcmMaker.prototype.defineRemoveQuestion = function() {
    $(".questions > .question > div > span > button[class='btn btn-danger']").off('click');
    $(".questions > .question > div > span > button[class='btn btn-danger']").click(function (event) {
        var questions = $(event.target.parentElement.parentNode.parentNode.parentNode).find(".question");

        if(questions.length > 1) {
            event.target.parentElement.parentNode.parentNode.remove();
        }
    });
};

//Suppression d'un choix
QcmMaker.prototype.defineRemoveChoice = function() {
    $(".choices > .choice button").off('click');
    $(".choices > .choice button").click(function (event) {
        var choices = $(event.target.parentElement.parentNode.parentNode.parentNode).find(".choice");

        if(choices.length > 1) {
            event.target.parentElement.parentNode.parentNode.remove();
        }
    });
};

QcmMaker.prototype.submitFormCreateQCM = function() {

    var qcm = { questions: [], titre: $("#titleQCM").val()};
    var listQuestions = $(".questions > .question");

    for(var i = 0; i < listQuestions.length; i++) {

        var dataChoices = [];
        var listChoices = $(listQuestions[i]).find(".choices > .choice");

        for(var j = 0; j < listChoices.length; j++) {
            dataChoices.push({
                counter: 0,
                content: $(listChoices[j]).find("input").val()
            });
        }
        qcm.questions.push({
            content: $(listQuestions[i]).find(".form-group > input").val(),
            choices: dataChoices
        });
    }

    if(this.id != null) {
        qcm.id = this.id;
    }

    this.titre = $("#titleQCM").val();

    var self = this;
    $.ajax({
        url: this.context + (this.id != null ? ('/qcm/' + this.id) : '/qcm'),
        type: this.id != null ? 'put' : 'post',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if(self.id == null) {
                self.id = data.id;
                self.load = true;
                self.initQRCode();
                self.getQCM();
                self.showModal();
            }

        },
        data: JSON.stringify(qcm)
    });

    return false;
};