var ChartQCM = function(element) {
    this.element = element;
    this.keys = [];
    this.values = [];
    this.isInit = false;
    this.createQuestionContent();
    this.createCanvas();
};

ChartQCM.prototype.createQuestionContent = function() {
    var h2 = document.createElement("h2");
    this.questionContent = document.createTextNode("");
    h2.className = "text-center";
    h2.appendChild(this.questionContent);
    this.element.appendChild(h2);
};

ChartQCM.prototype.createCanvas = function() {
    this.canvas = document.createElement("canvas");
    this.element.appendChild(this.canvas);
};

ChartQCM.prototype.initChart = function(){
    this.myChart = new Chart(this.canvas, {
        type: 'bar',
        data: {
            labels: this.keys,
            datasets: [{
                label: 'Réponses',
                data: this.values,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
};


ChartQCM.prototype.initKeys = function(data){
    this.keys = data.choices.map(function(element) { return element.content });
};

ChartQCM.prototype.initValues = function(data){
    this.values = data.choices.map(function(element) { return element.counter });
};

ChartQCM.prototype.updateChart = function(data){
    this.initKeys(data);
    this.initValues(data);
    if(this.isInit === false) {
        this.initChart();
        this.isInit = true;
    }
    this.myChart.data.datasets[0].data = this.values;
    this.myChart.data.keys = this.keys;
    this.myChart.data.labels = this.keys;
    this.myChart.update();

    this.questionContent.textContent = data.content;
};