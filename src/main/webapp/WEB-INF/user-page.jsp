<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />

<t:wrapper>
    <script src="${context}/ressources/js/qcmDeleter.js"></script>
    <t:qcm-delete-message/>
    <div class="profile-page sidebar-collapse">
        <t:navbar/>
        <div class="page-header header-filter" data-parallax="true" style="background-image: url('${context}/ressources/where-to-learn.png');"></div>
        <div class="main main-raised profile-main" style="display: flex; flex-direction: column; justify-content: space-between;">
            <div class="profile-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 ml-auto mr-auto">
                            <div class="profile">
                                <div class="name">
                                    <h3 class="title" style="color: white;">
                                        <c:out value="${requestScope.username}"/>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="description text-center">
                        <p>Ici vous trouverez les divers QCM que vous avez créé!<br>
                            Chaque QCM a un lien pour le modifier et un lien de partage pour recevoir des reponses.<br>
                            Et quelques autres fonctionalités a venir ;) </p>
                    </div>
                    <c:choose>
                        <c:when test="${requestScope.qcmList != null}">
                            <div class="row">
                                <div class="col-md-6 ml-auto mr-auto">
                                    <div class="profile-tabs">
                                       <c:forEach items="${requestScope.qcmList}" var="iterator">
                                            <ul class="nav nav-pills nav-pills-icons justify-content-center" role="tablist">
                                                <li class="nav-item">
                                                    <h4 class="tim-typo text-center"><c:out value="${iterator.titre}"/> : </h4>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="${context}/qcm/${iterator.id}/edit" class="nav-link active">
                                                        <em class="material-icons">edit</em>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="${context}/qcm/${iterator.id}" class="nav-link active">
                                                        <em class="material-icons">insert_link</em>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <button type="button" class="nav-link active btn-warning"
                                                            onclick='createDelete(this,"${context}","${iterator.id}");'>
                                                        <em class="material-icons">delete</em>
                                                    </button>
                                                </li>
                                            </ul>
                                       </c:forEach>
                                    </div>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="container">
                                <h2 class="tim-typo text-center">
                                    Aucun qcm créé :(
                                </h2>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <div class="footer text-center">
                <div class="container">
                    <a href="${context}/qcm" class="btn btn-danger btn-raised btn-lg">
                        <em class="fa fa-play"></em> Créer QCM
                    </a>
                </div>
                <div class="container">
                    <a href="${context}/deco" class="btn btn-link">Deconnexion</a>
                </div>
            </div>
        </div>
    </div>
</t:wrapper>
