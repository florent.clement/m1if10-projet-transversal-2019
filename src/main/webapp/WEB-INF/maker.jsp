<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<jsp:useBean id="model" class="fr.univ.lyon1.model.Qcm" scope="request"/>
<c:set var="context" value="${pageContext.request.contextPath}" />
<c:set var="qcmAtt" value='<%= request.getAttribute("qcmAtt") %>'/>
<c:set var="qrBase" value='<%= request.getRequestURL().toString().replace(request.getRequestURI(),"") %>' />

<t:wrapper>
    <div class="profile-page sidebar-collapse">
        <div class="page-header header-filter" data-parallax="true" style="background-image: url('${context}/ressources/where-to-learn.png');"></div>
        <div class="main main-raised col-md-8 ml-auto mr-auto">
            <div class="row">
                <div class="col-md-auto ml-auto mr-auto">
                    <div class="profile-tabs">
                        <ul class="nav nav-pills nav-pills-icons justify-content-center" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="#edit" role="tab" data-toggle="tab">
                                    <em class="material-icons">edit</em> Editer
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#qrcode" role="tab" data-toggle="tab">
                                    <em class="fa fa-qrcode" aria-hidden="true"></em> QR code
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#graph" role="tab" data-toggle="tab">
                                    <em class="material-icons">bar_chart</em> Graphe
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="profile-content">
                <div class="container tab-content">
                    <div class="tab-pane active row" id="edit">
                        <t:qcm-maker-edit/>
                    </div>
                    <div class="tab-pane gallery" id="graph">
                        <t:qcm-maker-graph/>
                    </div>
                    <div class="tab-pane gallery" id="qrcode">
                        <t:qcm-maker-qrcode/>
                    </div>
                </div>
            </div>
            <footer>
                <div class="container text-center">
                    <a href="${context}/" class="btn btn-primary btn-link">
                        Retourner a la page principale
                    </a>
                </div>
            </footer>
        </div>
        <div class="container">
            <t:navbar/>
        </div>
    </div>
<br><br>
<script src="${context}/ressources/js/chartQCM.js"></script>
<script src="${context}/ressources/js/qcmMaker.js"></script>
<script>
    var qcmMaker = new QcmMaker("${context}", "${qrBase}"<c:if test="${not empty model}">
            <c:out value=", ${model.id}" default="null"/>
        </c:if>);
</script>
</t:wrapper>
