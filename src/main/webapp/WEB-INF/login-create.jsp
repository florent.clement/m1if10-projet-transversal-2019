<%@page contentType="text/html;charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />

<t:wrapper>
    <div class="landing-page sidebar-collapse">
        <t:navbar/>
        <div class="page-header header-filter" data-parallax="true" style="background-image: url('${context}/ressources/where-to-learn.png');">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 ml-auto mr-auto">
                        <div class="card card-login">
                            <form class="form" method="post" action="${context}/login">
                                <input type="hidden" name="action" value="create">
                                <div class="card-header card-header-primary text-center">
                                    <h4 class="card-title">Inscrivez Vous</h4>
                                    <div class="social-line">
                                        <button type="button"  class="btn btn-just-icon btn-link">
                                            <em class="fa fa-facebook-square"></em>
                                        </button>
                                        <button type="button"  class="btn btn-just-icon btn-link">
                                            <em class="fa fa-twitter"></em>
                                        </button>
                                        <button type="button" class="btn btn-just-icon btn-link">
                                            <em class="fa fa-google-plus"></em>
                                        </button>
                                    </div>
                                </div>
                                <p class="description text-center">Ou à l'ancienne</p>
                                <div class="card-body">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                              <em class="material-icons">face</em>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Nom d'utilisateur..."
                                               name="username" pattern="([A-Z]|[a-z]|[0-9])+" required>
                                    </div>
                                    <c:if test='${param.exists != null}'>
                                        <div class="container">
                                            <span style="color:red;">
                                                <em class="material-icons">error</em> Ce nom d'utilisateur éxiste déjà
                                            </span>
                                        </div>
                                    </c:if>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                              <em class="material-icons">lock_outline</em>
                                            </span>
                                        </div>
                                        <input type="password" class="form-control" placeholder="Mot de passe..."
                                               name="password" pattern="([A-Z]|[a-z]|[0-9])+" required>
                                    </div>
                                </div>
                                <br>
                                <div class="footer text-center" style="display: flex; flex-flow: column;">
                                    <div class="container">
                                        <button type="submit" class="btn btn-primary">Inscrivez Vous!</button>
                                    </div>
                                    <div class="container">
                                        <a href="${context}/login" class="btn btn-primary btn-link btn-wd btn-lg">Connexion</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</t:wrapper>