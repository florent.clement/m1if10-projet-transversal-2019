<%@tag description="User Page template" pageEncoding="UTF-8"%>
<div class="modal fade" id="qcm-delete-message" tabindex="-1" role="dialog" aria-labelledby="qcm-message-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="qcm-message-label">Qcm enregistré !</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <p>Voulez vous supprimer ce QCM?</p>
                <button id="delete-button" type="button" class="btn btn-primary btn-danger">Oui</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>