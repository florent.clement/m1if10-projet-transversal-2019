<%@tag description="User Page template" pageEncoding="UTF-8"%>
<div class="modal fade" id="qcm-message" tabindex="-1" role="dialog" aria-labelledby="qcm-message-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="qcm-message-label">Qcm enregistré !</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Votre Qcm a bien été pris en compte, vous pouvez le partager en utilisant le QRcode dans la rubrique prévu à cet effet ou en partageant le code suivant :</p>
                <h5 id="qcm-message-link" style="font-weight: bold"></h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>