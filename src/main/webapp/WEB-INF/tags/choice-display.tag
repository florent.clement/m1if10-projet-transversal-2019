<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="User Page template" pageEncoding="UTF-8"%>
<%@attribute name="choice" required="true" type="fr.univ.lyon1.model.Choice"%>
<div class="form-check">
    <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="${choice.id}"
               id="${choice.id}" value="<c:out value="${choice.content}"/>">
        <c:out value="${choice.content}"/>
        <span class="form-check-sign">
            <span class="check"></span>
        </span>
    </label>
</div>
