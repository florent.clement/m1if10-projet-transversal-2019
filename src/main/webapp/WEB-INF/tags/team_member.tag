<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="User Page template" pageEncoding="UTF-8"%>
<%@attribute name="username" required="true" type="java.lang.String"%>
<%@attribute name="link" required="true" type="java.lang.String"%>
<%@attribute name="avatar" required="true" type="java.lang.String"%>
<%@attribute name="role" required="true" type="java.lang.String"%>
<%@attribute name="description" required="true" type="java.lang.String"%>
<div class="col-md-4">
    <div class="team-player">
        <div class="card card-plain">
            <div class="col-md-6 ml-auto mr-auto">
                <img src="${avatar}" alt="profile" class="img-raised rounded-circle img-fluid">
            </div>
            <h4 class="card-title"> ${username}
                <br>
                <small class="card-description text-muted">${role}</small>
            </h4>
            <div class="card-body">
                <p class="card-description">${description}</p>
            </div>
            <div class="card-footer justify-content-center">
                <a href="${link}" target="_blank" class="btn btn-link btn-just-icon"><i class="fa fa-gitlab"></i></a>
            </div>
        </div>
    </div>
</div>