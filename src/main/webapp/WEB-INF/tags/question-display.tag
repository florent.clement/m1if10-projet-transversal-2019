<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@tag description="User Page template" pageEncoding="UTF-8"%>
<%@attribute name="question" required="true" type="fr.univ.lyon1.model.Question"%>
<div class="tim-typo" style="padding-left: 0; padding-bottom: 0;margin-bottom: 0">
    <h3>
        <c:out value="${question.content}" />
    </h3>
</div>
<hr>
<c:forEach items="${ question.choiceList }" var="choice">
    <t:choice-display choice="${choice}"/>
</c:forEach>