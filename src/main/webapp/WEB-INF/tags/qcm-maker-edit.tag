<%@tag description="User Page template" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<style>
    .choice {
        padding-bottom: 10px;
    }
</style>
<h1 class="tim-typo text-center">Création de QCM</h1>
<div class="col-md-6 ml-auto mr-auto" id="createContent">
    <form onsubmit="return qcmMaker.submitFormCreateQCM()">
        <div class="form-group bmd-form-group">
            <label class="bmd-label-static">Titre du QCM</label>
            <input id="titleQCM" type="text" class="form-control" placeholder="Entrer le titre ici">
        </div>
        <br><br>
        <section class="questions">
            <button type="button" class="btn btn-success">+ question</button>
        </section>
        <br><br>
        <button type="submit" id="validQCM" class="btn btn-primary">Soumettre</button><br>
    </form>
</div>
<t:qcm-maker-edit-message/>