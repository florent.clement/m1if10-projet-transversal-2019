<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />

<t:wrapper>
    <div class="landing-page sidebar-collapse">
        <t:navbar/>
        <div class="page-header header-filter" data-parallax="true" style="background-image: url('${context}/ressources/where-to-learn.png')">
            <div class="container text-center">
                <h1 class="title">:O Un problème s'est produit lors de votre requête :/</h1>
                <h4>Nos devs sont en train de travailler pour règler ces problèmes :)</h4>
                <br><br>
                <div class="container text-center">
                    <a href="${context}/" class="btn btn-warning btn-raised btn-lg btn-link">
                        <em class="material-icons">home_work</em> Revenir à la page d'acceuil
                    </a>
                </div>
            </div>
        </div>
    </div>
</t:wrapper>
