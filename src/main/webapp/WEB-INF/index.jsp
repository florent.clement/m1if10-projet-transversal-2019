<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />

<t:wrapper>
<div class="landing-page sidebar-collapse">
    <t:navbar/>
    <div class="page-header header-filter" data-parallax="true" style="background-image: url('${context}/ressources/where-to-learn.png')">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-8">
                    <h1 class="title">Un QCM pour mieux apprendre</h1>
                    <h4>Nous vous proposons un large de choix de QCM sur ce site accessible via un identifiant ou un QR code</h4>
                    <br>
                    <div class="row d-flex justify-content-center">
                        <div class="col-auto col-md-6">
                            <a href="${context}/qcm" class="btn btn-danger btn-raised btn-lg">
                                <em class="fa fa-play"></em> Créer QCM
                            </a>
                        </div>
                        <div class="col-auto col-md-6 form-inline">
                            <div class="form-group has-white">
                                <input id="searchQCM" type="text" class="form-control text-white" placeholder="Identifiant QCM">
                            </div>
                            <button type="submit" class="btn btn-white btn-raised btn-fab btn-round"
                                    onclick="window.location='${context}/qcm/' + document.getElementById('searchQCM').value;">
                            <em class="material-icons">search</em>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main main-raised">
        <div class="container">
            <div class="section text-center">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto">
                        <h2 class="title">A propos du produit</h2>
                        <h5 class="description">AmphiQuest est une application web offrant la possibilité de créer des QCM et de consulter en temps réel les réponses des participants. Elle s'adresse principalement aux enseignants mais également à toute personne souhaitant partager un sondage. De plus AmphiQuest est entièrement gratuit alors lancez-vous ! </h5>
                    </div>
                </div>
            </div>
            <div class="section text-center">
                <h2 class="title">Notre équipe</h2>
                <div class="team">
                    <div class="row">
                            <t:team_member
                                    username="Jose VILLAMAR"
                                    link="https://forge.univ-lyon1.fr/p1511502"
                                    avatar="https://secure.gravatar.com/avatar/de41ab5349624e8187a678fab4d80b86?s=180&d=identicon"
                                    role="Scrum Master"
                                    description="Etudiant en 1ère année de Master Informatique à l'université Claude Bernard Lyon 1."/>
                            <t:team_member
                                    username="Sam BEKKIS"
                                    link="https://forge.univ-lyon1.fr/p1610426"
                                    avatar="https://secure.gravatar.com/avatar/85ad9c8d9d1b8bd73fad2e7b629e548f?s=180&d=identicon"
                                    role="Product Owner"
                                    description="Etudiant en 1ère année de Master Informatique à l'université Claude Bernard Lyon 1."/>
                            <t:team_member
                                    username="Virgile ARAGON"
                                    link="https://forge.univ-lyon1.fr/p1507912"
                                    avatar="https://secure.gravatar.com/avatar/59c99f72e5e9d9e40dac35cf279794cb?s=180&d=identicon"
                                    role="Responsable qualité"
                                    description="Etudiant en 1ère année de Master Informatique à l'université Claude Bernard Lyon 1."/>
                            <t:team_member
                                    username="Nabil LAMRABET"
                                    link="https://forge.univ-lyon1.fr/p1806768"
                                    avatar="https://forge.univ-lyon1.fr/uploads/-/system/user/avatar/1708/avatar.png"
                                    role="Tracker"
                                    description="Etudiant en 1ère année de Master Informatique à l'université Claude Bernard Lyon 1."/>
                            <t:team_member
                                    username="Pablo CHABANNE"
                                    link="https://forge.univ-lyon1.fr/p1602176"
                                    avatar="https://secure.gravatar.com/avatar/1375ba5e6304372b2543189858808896?s=180&d=identicon"
                                    role="Référent technique"
                                    description="Etudiant en 1ère année de Master Informatique à l'université Claude Bernard Lyon 1."/>
                            <t:team_member
                                    username="Florent CLEMENT"
                                    link="https://forge.univ-lyon1.fr/p1601511"
                                    avatar="https://forge.univ-lyon1.fr/uploads/-/system/user/avatar/1702/avatar.png"
                                    role="Chef d'équipe"
                                    description="Etudiant en 1ère année de Master Informatique à l'université Claude Bernard Lyon 1."/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer footer-default">
        <div class="container">
            <nav class="float-left">
                <ul>
                    <li>

                    </li>
                </ul>
            </nav>
        </div>
    </footer>
</div>
</t:wrapper>
