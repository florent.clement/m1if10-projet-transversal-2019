<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<jsp:useBean id="model" class="fr.univ.lyon1.model.Qcm" scope="request"/>

<t:wrapper>
    <div class="profile-page sidebar-collapse">
        <t:navbar/>
        <div class="page-header header-filter" data-parallax="true" style="background-image: url('${context}/ressources/where-to-learn.png');"></div>
        <div class="main main-raised col-md-8 ml-auto mr-auto">
            <div class="profile-content">
                <div class="container">
                    <div class="row">
                        <h1 class="tim-typo text-center"><c:out value="${model.titre}"/></h1>
                        <div class="col-md-6 ml-auto mr-auto">
                            <form method="post" action="${context}/qcm/${model.id}">
                                <c:forEach items="${ model.questionList }" var="question">
                                    <t:question-display question="${question}"/>
                                </c:forEach>
                                <button type="submit" class="btn btn-success">Envoyer</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</t:wrapper>