<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<c:set var="context" value="${pageContext.request.contextPath}" />

<t:wrapper>
    <div class="landing-page sidebar-collapse">
        <t:navbar/>
        <div class="page-header header-filter" data-parallax="true" style="background-image: url('${context}/ressources/where-to-learn.png')">
            <div class="container text-center">
                <h1 class="title">Merci pour votre réponse !</h1>
                <h4>Vous pouvez créer un qcm par vous mêmes aussi... Ou réponde à un autre :)</h4>
                <br><br>
                <div class="row d-flex justify-content-around">
                    <div class="col-auto col-md-6">
                        <a href="${context}/qcm" class="btn btn-danger btn-raised btn-lg">
                            <em class="fa fa-play"></em> Créer QCM
                        </a>
                    </div>
                    <div class="col-auto col-md-6">
                        <div class="form-inline justify-content-center">
                            <div class="form-group has-white">
                                <input id="searchQCM" type="text" class="form-control text-white" placeholder="Identifiant QCM">
                            </div>
                            <button type="submit" class="btn btn-white btn-raised btn-fab btn-round"
                                    onclick="window.location='${context}/qcm/' + document.getElementById('searchQCM').value;">
                                <em class="material-icons">search</em>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</t:wrapper>
