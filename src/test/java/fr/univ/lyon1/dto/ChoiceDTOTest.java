package fr.univ.lyon1.dto;

import fr.univ.lyon1.dao.DAOContext;
import fr.univ.lyon1.dao.QcmDAO;
import fr.univ.lyon1.dao.QuestionDAO;
import fr.univ.lyon1.model.Choice;
import fr.univ.lyon1.model.Qcm;
import fr.univ.lyon1.model.Question;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.HashMap;
import java.util.Map;

public class ChoiceDTOTest {

    private static Qcm qcm;

    private static Question question;

    @BeforeClass
    public static void initDAOContext() {
        DAOContext.init();

        qcm = new Qcm();
        QcmDAO.createOrUpdate(qcm);

        question = new Question();
        question.setContent("test");
        question.setQcm(qcm);

        QuestionDAO.createOrUpdate(question);
    }

    @Test
    public void getJsonStructure() {
        // Given
        Choice choice = new Choice();
        choice.setId(1L);
        choice.setQuestion(question);
        choice.setContent("content");
        choice.setCounter(0);

        JsonObject jsonObject;
        ChoiceDTO choiceDTO = BuilderDTO.buildDTO(choice,ChoiceDTO.class);

        // When
        jsonObject = choiceDTO.getJsonStructure();

        // Then
        Assert.assertEquals( 1L,Long.parseLong(String.valueOf(jsonObject.getInt("id"))));
        Assert.assertEquals(
                Long.parseLong(String.valueOf(jsonObject.getInt("idQuestion"))),
                (long)question.getId());

        Assert.assertEquals("content", jsonObject.getString("content"));
        Assert.assertEquals(0, jsonObject.getInt("counter"));
    }

    @Test
    public void parseJson() {
        // Given
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();

        jsonObjectBuilder.add("id",1);
        jsonObjectBuilder.add("content","content");
        jsonObjectBuilder.add("counter",0);
        jsonObjectBuilder.add("idQuestion",question.getId());

        JsonObject jsonObject = jsonObjectBuilder.build();
        Choice choice;
        ChoiceDTO choiceDTO = new ChoiceDTO();

        // When
        choice = choiceDTO.parseJson(jsonObject).build();

        // Then
        Assert.assertEquals(choice.getId(),(Long)1L);
        Assert.assertEquals("content",choice.getContent());
        Assert.assertEquals(0, choice.getCounter());
        Assert.assertNotNull(choice.getQuestion());
        Assert.assertEquals(choice.getQuestion().getId(),question.getId());
    }

    @Test
    public void getElementXML() {
        // Given
        Choice choice = new Choice();
        choice.setId(1L);
        choice.setQuestion(question);
        choice.setContent("content");
        choice.setCounter(0);

        ChoiceDTO choiceDTO = BuilderDTO.buildDTO(choice,ChoiceDTO.class);

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        Element element = null;
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            // When
            element = choiceDTO.getElementXML(document);

        } catch (ParserConfigurationException e) {
            Assert.fail();
        }
        // Then
        //id
        Assert.assertEquals( 1L, Long.parseLong(element.getElementsByTagName("id")
                .item(0).getTextContent())
        );

        //idQcm
        Assert.assertEquals(
                Long.parseLong(element.getElementsByTagName("idQuestion").item(0).getTextContent()),
                (long)question.getId());

        //content
        Assert.assertEquals("content", element.getElementsByTagName("content").item(0).getTextContent());

        //counter
        Assert.assertEquals(0, Integer.parseInt(element.getElementsByTagName("counter").item(0)
                .getTextContent())
        );

    }

    @Test
    public void parseXML() {
        // Given
        Choice choice = null;
        ChoiceDTO choiceDTO = new ChoiceDTO();
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            Element root = document.createElement("choice");
            //id
            Element id = document.createElement("id");
            id.appendChild(document.createTextNode(String.valueOf(1L)));
            root.appendChild(id);

            //idQuestion
            Element idQuestion = document.createElement("idQuestion");
            idQuestion.appendChild(document.createTextNode(String.valueOf(question.getId())));
            root.appendChild(idQuestion);

            //content
            Element content = document.createElement("content");
            content.appendChild(document.createTextNode("content"));
            root.appendChild(content);

            //counter
            Element counter = document.createElement("counter");
            counter.appendChild(document.createTextNode(String.valueOf(0)));
            root.appendChild(counter);
            document.appendChild(root);

            // When
            choice = choiceDTO.parseXML(document.getDocumentElement()).build();

        } catch (ParserConfigurationException e) {
            Assert.fail();
        }


        // Then
        Assert.assertNotNull(choice.getId());
        Assert.assertEquals(choice.getId(),(Long)1L);
        Assert.assertEquals("content", choice.getContent());
        Assert.assertEquals(0, choice.getCounter());
        Assert.assertNotNull(choice.getQuestion());
        Assert.assertEquals(choice.getQuestion().getId(),question.getId());
    }

    @Test
    public void parseFormData() {
        // Given
        Map<String, String> formDataContent = new HashMap<>();
        formDataContent.put("id", "1");
        formDataContent.put("idQuestion", question.getId().toString());
        formDataContent.put("content", "content");
        formDataContent.put("counter", "0");

        // When
        ChoiceDTO choiceDTO = new ChoiceDTO().parseFormData(formDataContent);

        Choice choice = choiceDTO.build();
        // Then
        Assert.assertNotNull(choiceDTO);
        Assert.assertNotNull(choice);
        Assert.assertEquals((Long) 1L, choice.getId());
        Assert.assertEquals(question.getId(), choice.getQuestion().getId());
        Assert.assertEquals("content", choice.getContent());
        Assert.assertEquals(0, choice.getCounter());
    }

    @Test
    public void build() {
        // Given
        Choice choice = new Choice();
        ChoiceDTO choiceDTO;

        // When
        choiceDTO = BuilderDTO.buildDTO(choice,ChoiceDTO.class);

        // Then
        Assert.assertEquals(choice,choiceDTO.build());
    }

    @AfterClass
    public static void removeQcm() {
        QcmDAO.delete(qcm);
    }
}