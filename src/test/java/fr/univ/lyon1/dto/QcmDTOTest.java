package fr.univ.lyon1.dto;

import fr.univ.lyon1.model.Qcm;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.HashMap;
import java.util.Map;

public class QcmDTOTest {

    @Test
    public void getJsonStructure() {
        // Given
        Qcm qcm = new Qcm();
        qcm.setId(1L);

        JsonObject jsonObject;
        QcmDTO qcmDTO = BuilderDTO.buildDTO(qcm,QcmDTO.class);

        // When
        jsonObject = qcmDTO.getJsonStructure();

        // Then
        Assert.assertEquals(1L, Long.parseLong(String.valueOf(jsonObject.getInt("id"))));
    }

    @Test
    public void parseJson() {
        // Given
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();

        jsonObjectBuilder.add("id",1);

        JsonObject jsonObject = jsonObjectBuilder.build();
        Qcm qcm;
        QcmDTO qcmDTO = new QcmDTO();

        // When
        qcm = qcmDTO.parseJson(jsonObject).build();

        // Then
        Assert.assertEquals(qcm.getId(),(Long)1L);
    }

    @Test
    public void getElementXML() {
        // Given
        Qcm qcm = new Qcm();
        qcm.setId(1L);

        QcmDTO qcmDTO = BuilderDTO.buildDTO(qcm,QcmDTO.class);

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        Element element = null;
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            // When
            element = qcmDTO.getElementXML(document);

        } catch (ParserConfigurationException e) {
            Assert.fail();
        }
        // Then
        //id
        Assert.assertEquals( 1L,
                Long.parseLong(element.getElementsByTagName("id").item(0).getTextContent()));
    }

    @Test
    public void parseXML() {
        // Given
        Qcm qcm = null;
        QcmDTO qcmDTO = new QcmDTO();
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            Element root = document.createElement("qcm");
            //id
            Element id = document.createElement("id");
            id.appendChild(document.createTextNode(String.valueOf(1L)));
            root.appendChild(id);

            document.appendChild(root);

            // When
            qcm = qcmDTO.parseXML(document.getDocumentElement()).build();

        } catch (ParserConfigurationException e) {
            Assert.fail();
        }


        // Then
        Assert.assertNotNull(qcm.getId());
        Assert.assertEquals(qcm.getId(),(Long)1L);
    }

    @Test
    public void parseFormData() {
        // Given
        Map<String, String> formDataContent = new HashMap<>();
        formDataContent.put("id", "1");
        formDataContent.put("titre", "titre");

        // When
        QcmDTO qcmDTO = new QcmDTO().parseFormData(formDataContent);

        Qcm qcm = qcmDTO.build();
        // Then
        Assert.assertNotNull(qcmDTO);
        Assert.assertNotNull(qcm);
        Assert.assertEquals((Long) 1L, qcm.getId());
        Assert.assertEquals("titre", qcm.getTitre());
    }

    @Test
    public void build() {
        // Given
        Qcm qcm = new Qcm();
        QcmDTO qcmDTO;

        // When
        qcmDTO = BuilderDTO.buildDTO(qcm,QcmDTO.class);

        // Then
        Assert.assertEquals(qcm,qcmDTO.build());
    }
}