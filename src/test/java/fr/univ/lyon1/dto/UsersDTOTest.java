package fr.univ.lyon1.dto;

import fr.univ.lyon1.model.Users;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import static org.junit.Assert.*;

public class UsersDTOTest {

    @Test
    public void getJsonStructure() {
        // Given
        Users user = new Users();
        user.setName("TEST");
        user.setPassword(123);

        JsonObject jsonObject;
        UsersDTO userDTO = BuilderDTO.buildDTO(user,UsersDTO.class);

        // When
        jsonObject = userDTO.getJsonStructure();

        // Then
        Assert.assertEquals("TEST", jsonObject.getString("name"));
        Assert.assertEquals(123, jsonObject.getInt("password"));
    }

    @Test
    public void parseJson() {
        // Given
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();

        jsonObjectBuilder.add("name","TEST");
        jsonObjectBuilder.add("password",123);

        JsonObject jsonObject = jsonObjectBuilder.build();
        Users user;
        UsersDTO userDTO = new UsersDTO();

        // When
        user = userDTO.parseJson(jsonObject).build();

        // Then
        Assert.assertNotNull(user.getName());
        Assert.assertEquals("TEST", user.getName());
        Assert.assertEquals(123, user.getPassword());
    }

    @Test
    public void getElementXML() {
        // Given
        Users user = new Users();
        user.setName("TEST");
        user.setPassword(123);

        UsersDTO userDTO = BuilderDTO.buildDTO(user,UsersDTO.class);

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        Element element = null;
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            // When
            element = userDTO.getElementXML(document);

        } catch (ParserConfigurationException e) {
            Assert.fail();
        }
        // Then
        Assert.assertEquals("TEST", element.getElementsByTagName("name").item(0).getTextContent());
        Assert.assertEquals(123, Integer.parseInt(element.getElementsByTagName("password")
                .item(0).getTextContent())
        );
    }

    @Test
    public void parseXML() {
        // Given
        Users user = null;
        UsersDTO userDTO = new UsersDTO();
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            Element root = document.createElement("user");
            //id
            Element name = document.createElement("name");
            name.appendChild(document.createTextNode("TEST"));
            root.appendChild(name);
            Element password = document.createElement("password");
            password.appendChild(document.createTextNode("123"));
            root.appendChild(password);

            document.appendChild(root);

            // When
            user = userDTO.parseXML(document.getDocumentElement()).build();

        } catch (ParserConfigurationException e) {
            Assert.fail();
        }


        // Then
        Assert.assertNotNull(user.getName());
        Assert.assertEquals("TEST", user.getName());
        Assert.assertEquals(123, user.getPassword());
    }

    @Test
    public void build() {
        // Given
        Users user = new Users("TEST", 123);
        UsersDTO userDTO;

        // When
        userDTO = BuilderDTO.buildDTO(user,UsersDTO.class);

        // Then
        Assert.assertEquals(user,userDTO.build());
    }
}