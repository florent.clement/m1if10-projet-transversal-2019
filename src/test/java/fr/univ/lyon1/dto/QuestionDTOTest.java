package fr.univ.lyon1.dto;

import fr.univ.lyon1.dao.DAOContext;
import fr.univ.lyon1.dao.QcmDAO;
import fr.univ.lyon1.model.Choice;
import fr.univ.lyon1.model.Qcm;
import fr.univ.lyon1.model.Question;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.HashMap;
import java.util.Map;

public class QuestionDTOTest {

    private static Qcm qcm;

    @BeforeClass
    public static void initDAOContext() {
        DAOContext.init();

        qcm = new Qcm();
        QcmDAO.createOrUpdate(qcm);
    }

    @Test
    public void getJsonStructure() {
        // Given
        Question question = new Question();
        question.setId(1L);
        question.setQcm(qcm);
        question.setContent("content");

        JsonObject jsonObject;
        QuestionDTO questionDTO = BuilderDTO.buildDTO(question,QuestionDTO.class);

        // When
        jsonObject = questionDTO.getJsonStructure();

        // Then
        Assert.assertEquals(1L, Long.parseLong(String.valueOf(jsonObject.getInt("id"))));
        Assert.assertEquals(
                Long.parseLong(String.valueOf(jsonObject.getInt("idQcm"))),
                (long)qcm.getId());

        Assert.assertEquals("content", jsonObject.getString("content"));
    }

    @Test
    public void parseJson() {
        // Given
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();

        jsonObjectBuilder.add("id",1);
        jsonObjectBuilder.add("content","content");
        jsonObjectBuilder.add("idQcm",qcm.getId());

        JsonObject jsonObject = jsonObjectBuilder.build();
        Question question;
        QuestionDTO questionDTO = new QuestionDTO();

        // When
        question = questionDTO.parseJson(jsonObject).build();

        // Then
        Assert.assertEquals(question.getId(),(Long)1L);
        Assert.assertEquals("content", question.getContent());
        Assert.assertNotNull(question.getQcm());
        Assert.assertEquals(question.getQcm().getId(),qcm.getId());
    }

    @Test
    public void getElementXML() {
        // Given
        Question question = new Question();
        question.setId(1L);
        question.setQcm(qcm);
        question.setContent("content");

        QuestionDTO questionDTO = BuilderDTO.buildDTO(question,QuestionDTO.class);

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        Element element = null;
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            // When
            element = questionDTO.getElementXML(document);

        } catch (ParserConfigurationException e) {
            Assert.fail();
        }
        // Then
        //id
        Assert.assertEquals( 1L, Long.parseLong(element.getElementsByTagName("id").item(0)
                .getTextContent())
        );

        //idQcm
        Assert.assertEquals(
                Long.parseLong(element.getElementsByTagName("idQcm").item(0).getTextContent()),
                (long)qcm.getId());

        //content
        Assert.assertEquals("content",
                element.getElementsByTagName("content").item(0).getTextContent());
    }

    @Test
    public void parseXML() {
        // Given
        Question question = null;
        QuestionDTO questionDTO = new QuestionDTO();
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            Element root = document.createElement("question");
            //id
            Element id = document.createElement("id");
            id.appendChild(document.createTextNode(String.valueOf(1L)));
            root.appendChild(id);

            //idQcm
            Element idQcm = document.createElement("idQcm");
            idQcm.appendChild(document.createTextNode(String.valueOf(qcm.getId())));
            root.appendChild(idQcm);

            //content
            Element content = document.createElement("content");
            content.appendChild(document.createTextNode("content"));
            root.appendChild(content);
            document.appendChild(root);

            // When
            question = questionDTO.parseXML(document.getDocumentElement()).build();

        } catch (ParserConfigurationException e) {
            Assert.fail();
        }


        // Then
        Assert.assertNotNull(question.getId());
        Assert.assertEquals(question.getId(),(Long)1L);
        Assert.assertEquals("content", question.getContent());
        Assert.assertNotNull(question.getQcm());
        Assert.assertEquals(question.getQcm().getId(),qcm.getId());
    }

    @Test
    public void parseFormData() {
        // Given
        Map<String, String> formDataContent = new HashMap<>();
        formDataContent.put("id", "1");
        formDataContent.put("idQcm", qcm.getId().toString());
        formDataContent.put("content", "content");

        // When
        QuestionDTO questionDTO = new QuestionDTO().parseFormData(formDataContent);

        Question question = questionDTO.build();
        // Then
        Assert.assertNotNull(questionDTO);
        Assert.assertNotNull(question);
        Assert.assertEquals((Long) 1L, question.getId());
        Assert.assertEquals(qcm.getId(), question.getQcm().getId());
        Assert.assertEquals("content", question.getContent());
    }

    @Test
    public void build() {
        // Given
        Question question = new Question();
        QuestionDTO questionDTO;

        // When
        questionDTO = BuilderDTO.buildDTO(question,QuestionDTO.class);

        // Then
        Assert.assertEquals(question,questionDTO.build());
    }

    @AfterClass
    public static void removeQcm() {
        QcmDAO.delete(qcm);
    }
}