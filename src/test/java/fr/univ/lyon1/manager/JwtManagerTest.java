package fr.univ.lyon1.manager;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpSession;

public class JwtManagerTest {

    @Test
    public void createOrUpdateSessionToken() {
        // Given
        HttpSession httpSession = Mockito.mock(HttpSession.class);
        JwtManager jwtManager = new JwtManager("user");

        // Then
        jwtManager.createOrUpdateSessionToken(httpSession);

        Mockito.verify(httpSession,Mockito.times(1))
                .setAttribute("token",jwtManager.getToken());
    }

    @Test
    public void getTokenSubject() {
        // Given
        JwtManager jwtManager = new JwtManager("user");
        String token = jwtManager.getToken();
        HttpSession httpSession = Mockito.mock(HttpSession.class);
        Mockito.when(httpSession.getAttribute("token")).thenReturn(token);

        //When
        String subjet = JwtManager.getTokenSubject(httpSession);

        // Then
        Assert.assertEquals("user",subjet);
    }
}