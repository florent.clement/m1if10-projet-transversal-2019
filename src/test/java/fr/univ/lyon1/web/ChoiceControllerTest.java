package fr.univ.lyon1.web;

import fr.univ.lyon1.dao.ChoiceDAO;
import fr.univ.lyon1.dao.DAOContext;
import fr.univ.lyon1.dao.QcmDAO;
import fr.univ.lyon1.dao.QuestionDAO;
import fr.univ.lyon1.model.Choice;
import fr.univ.lyon1.model.Qcm;
import fr.univ.lyon1.model.Question;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

public class ChoiceControllerTest {

    private static Qcm qcm;

    private static Question question;

    private static Choice choice;

    @BeforeClass
    public static void initChoice() {
        DAOContext.init();

        qcm = new Qcm();
        QcmDAO.createOrUpdate(qcm);

        question = new Question();
        question.setContent("test");
        question.setQcm(qcm);

        QuestionDAO.createOrUpdate(question);

        choice = new Choice();
        choice.setQuestion(question);
        ChoiceDAO.createOrUpdate(choice);
    }

    @Test
    public void doGet() {

        HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);
        HttpServletResponse httpServletResponse = Mockito.mock(HttpServletResponse.class);

        Mockito.when(httpServletRequest.getRequestURI()).thenReturn(
                "qcm/" + qcm.getId() + "/questions/"  + question.getId() + "/choices/" + choice.getId());
        Mockito.when(httpServletRequest.getContextPath()).thenReturn("");

        try {
            new ChoiceController().doGet(httpServletRequest,httpServletResponse);
        } catch (IOException e) {
            Assert.fail();
        }
    }
}