package fr.univ.lyon1.web;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class RegexTest {

    @Test
    public void matcher() {
        /*
        HOME("^/?"),

        RESSOURCES("^/ressources/*"),
        ASSETS("^/assets/*"),

        LOGIN_ROUTER("^/login/*"),
        LOGIN("^/login/?"),

        QCM_ROUTER ("^/qcm/*"),
        QCM ("^/qcm/?"),
        QCM_ID("^/qcm/([0-9]+)/?"),
        QCM_EDIT("^/qcm/([0-9]+)/edit/?"),

        QUESTIONS_ROUTER("^/qcm/([0-9]+)/questions/*"),
        QUESTIONS("^/qcm/([0-9]+)/questions/?"),
        QUESTIONS_ID("^/qcm/([0-9]+)/questions/([0-9]+)/?"),

        CHOICE_ROUTER("^/qcm/([0-9]+)/questions/([0-9]+)/choices/*"),
        CHOICE("^/qcm/([0-9]+)/questions/([0-9]+)/choices/?"),
        CHOICE_ID("^/qcm/([0-9]+)/questions/([0-9]+)/choices/([0-9]+)/?"),

        USER_ROUTER("^/user/.*"),
        USER("^/user/?"),

        DECO_ROUTER("^/deco/*"),
        DECO("^/deco/?");
        */

        Assert.assertTrue(Regex.HOME.matcher("/").matches()
                && !Regex.HOME.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.RESSOURCES.matcher("/ressources/").matches()
                && !Regex.RESSOURCES.matcher("/").matches()
                && !Regex.RESSOURCES.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.ASSETS.matcher("/assets/").matches()
                && !Regex.ASSETS.matcher("/").matches()
                && !Regex.ASSETS.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.LOGIN_ROUTER.matcher("/login").matches()
                && !Regex.LOGIN_ROUTER.matcher("/").matches()
                && !Regex.LOGIN_ROUTER.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.LOGIN.matcher("/login").matches()
                && !Regex.LOGIN.matcher("/login?name=value&name=value").matches()
                && !Regex.LOGIN.matcher("/").matches()
                && !Regex.LOGIN.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.QCM_ROUTER.matcher("/qcm").matches()
                && !Regex.QCM_ROUTER.matcher("/qcm/123/").matches()
                && !Regex.QCM_ROUTER.matcher("/").matches()
                && !Regex.QCM_ROUTER.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.QCM_EDIT.matcher("/qcm/123/edit").matches()
                && !Regex.QCM_EDIT.matcher("/qcm/123").matches()
                && !Regex.QCM_EDIT.matcher("/qcm/blabla/edit").matches()
                && !Regex.QCM_EDIT.matcher("/").matches()
                && !Regex.QCM_EDIT.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.QCM_ID.matcher("/qcm/123").matches()
                && !Regex.QCM_ID.matcher("/qcm/123/edit").matches()
                && !Regex.QCM_ID.matcher("/qcm/blabla").matches()
                && !Regex.QCM_ID.matcher("/").matches()
                && !Regex.QCM_ID.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.QCM.matcher("/qcm").matches()
                && !Regex.QCM.matcher("/qcm/123").matches()
                && !Regex.QCM.matcher("/qcm/123/edit").matches()
                && !Regex.QCM.matcher("/").matches()
                && !Regex.QCM.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.QUESTIONS_ROUTER.matcher("/qcm/123/questions").matches()
                && !Regex.QUESTIONS_ROUTER.matcher("/qcm/123/questions/456").matches()
                && !Regex.QUESTIONS_ROUTER.matcher("/questions").matches()
                && !Regex.QUESTIONS_ROUTER.matcher("/questions/456").matches()
                && !Regex.QUESTIONS_ROUTER.matcher("/qcm/questions").matches()
                && !Regex.QUESTIONS_ROUTER.matcher("/qcm/123").matches()
                && !Regex.QUESTIONS_ROUTER.matcher("/213/questions/465").matches()
                && !Regex.QUESTIONS_ROUTER.matcher("/").matches()
                && !Regex.QUESTIONS_ROUTER.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.QUESTIONS_ID.matcher("/qcm/123/questions/456/").matches()
                && !Regex.QUESTIONS_ID.matcher("/qcm/123/questions/blabla/").matches()
                && !Regex.QUESTIONS_ID.matcher("/qcm/123/questions").matches()
                && !Regex.QUESTIONS_ID.matcher("/question/456").matches()
                && !Regex.QUESTIONS_ID.matcher("/qcm/questions/456").matches()
                && !Regex.QUESTIONS_ID.matcher("/321/questions/654").matches()
                && !Regex.QUESTIONS_ID.matcher("/qcm/213").matches()
                && !Regex.QUESTIONS_ID.matcher("/").matches()
                && !Regex.QUESTIONS_ID.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.QUESTIONS.matcher("/qcm/321/questions").matches()
                && !Regex.QUESTIONS.matcher("/qcm").matches()
                && !Regex.QUESTIONS.matcher("/qcm/123").matches()
                && !Regex.QUESTIONS.matcher("/questions").matches()
                && !Regex.QUESTIONS.matcher("/qcm/questions").matches()
                && !Regex.QUESTIONS.matcher("/987/questions").matches()
                && !Regex.QUESTIONS.matcher("/qcm/987/questions/213").matches()
                && !Regex.QUESTIONS.matcher("/").matches()
                && !Regex.QUESTIONS.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.CHOICE_ROUTER.matcher("/qcm/321/questions/654/choices").matches()
                && !Regex.CHOICE_ROUTER.matcher("/qcm/321/questions/654/choices/456").matches()
                && !Regex.CHOICE_ROUTER.matcher("/qcm/321/questions/choices/456").matches()
                && !Regex.CHOICE_ROUTER.matcher("/qcm/654/choices/456").matches()
                && !Regex.CHOICE_ROUTER.matcher("/321/questions/654/choices/456").matches()
                && !Regex.CHOICE_ROUTER.matcher("/qcm/321/questions/456").matches()
                && !Regex.CHOICE_ROUTER.matcher("/").matches()
                && !Regex.CHOICE_ROUTER.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.CHOICE_ID.matcher("/qcm/321/questions/654/choices/123").matches()
                && !Regex.CHOICE_ID.matcher("/qcm/321/questions/654/choices").matches()
                && !Regex.CHOICE_ID.matcher("/qcm/321/questions/choices").matches()
                && !Regex.CHOICE_ID.matcher("/qcm/654/choices").matches()
                && !Regex.CHOICE_ID.matcher("/321/questions/654/choices").matches()
                && !Regex.CHOICE_ID.matcher("/").matches()
                && !Regex.CHOICE_ID.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.CHOICE.matcher("/qcm/321/questions/654/choices/").matches()
                && !Regex.CHOICE.matcher("/qcm/321/questions/654/choices/132").matches()
                && !Regex.CHOICE.matcher("/qcm/321/questions/choices/").matches()
                && !Regex.CHOICE.matcher("/qcm//654/choices/").matches()
                && !Regex.CHOICE.matcher("/questions/654/choices/132").matches()
                && !Regex.CHOICE.matcher("/").matches()
                && !Regex.CHOICE.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.USER_ROUTER.matcher("/user/").matches()
                && !Regex.USER_ROUTER.matcher("/").matches()
                && !Regex.USER_ROUTER.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.USER.matcher("/user").matches()
                && !Regex.USER.matcher("/").matches()
                && !Regex.USER.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.DECO_ROUTER.matcher("/deco/").matches()
                && !Regex.DECO_ROUTER.matcher("/").matches()
                && !Regex.DECO_ROUTER.matcher("/nimportequoi").matches());

        Assert.assertTrue(Regex.DECO.matcher("/deco").matches()
                && !Regex.DECO.matcher("/").matches()
                && !Regex.DECO.matcher("/nimportequoi").matches());

    }
}