package fr.univ.lyon1.dao;

import fr.univ.lyon1.model.Choice;
import fr.univ.lyon1.model.Qcm;
import fr.univ.lyon1.model.Question;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class ChoiceDAOTest {

    private static Qcm qcm;

    private static Question question;

    @BeforeClass
    public static void initDAOContext() {
        DAOContext.init();

        qcm = new Qcm();
        QcmDAO.createOrUpdate(qcm);

        question = new Question();
        question.setContent("test");
        question.setQcm(qcm);

        QuestionDAO.createOrUpdate(question);
    }

    @Test
    public void save() {
        // Given
        Choice choice = new Choice();
        choice.setContent("test");
        choice.setQuestion(question);

        // When
        ChoiceDAO.createOrUpdate(choice);

        // Then
        Assert.assertNotNull(choice.getId());
    }

    @Test
    public void update() {
        // Given
        Choice choice = new Choice();
        choice.setContent("test");
        choice.setQuestion(question);
        ChoiceDAO.createOrUpdate(choice);

        Assert.assertNotNull(choice.getId());

        // When
        choice.setContent("nouveau");
        ChoiceDAO.createOrUpdate(choice);

        // Then
        Assert.assertNotNull(choice);
        Assert.assertNotEquals(choice.getContent(),"test");
    }

    @Test
    public void delete() {
        // Given
        Choice choice = new Choice();
        choice.setContent("test");
        choice.setQuestion(question);
        ChoiceDAO.createOrUpdate(choice);

        Assert.assertNotNull(choice.getId());
        Long id = choice.getId();

        // When
        ChoiceDAO.delete(choice);

        // Then
        Assert.assertNull(ChoiceDAO.getById(id));

    }

    @Test
    public void getById() {
        // Given
        Choice choice = new Choice();
        choice.setContent("test");
        choice.setQuestion(question);

        // When
        ChoiceDAO.createOrUpdate(choice);

        // Then
        Assert.assertNotNull(choice.getId());

        Assert.assertNull(ChoiceDAO.getById(0L));
        Assert.assertNotNull(ChoiceDAO.getById(choice.getId()));
    }

    @AfterClass
    public static void removeQcm() {
        QcmDAO.delete(qcm);
    }
}