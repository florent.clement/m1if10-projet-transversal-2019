package fr.univ.lyon1.dao;

import org.junit.*;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class DAOContextTest {

    @Test
    public void setupEMTest() {
        EntityManager em = Persistence.createEntityManagerFactory("bdd").createEntityManager();
        Assert.assertNotNull(em);
        em.close();
    }
}