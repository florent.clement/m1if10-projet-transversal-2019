package fr.univ.lyon1.dao;

import fr.univ.lyon1.model.Qcm;
import fr.univ.lyon1.model.Question;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class QuestionDAOTest {

    private static Qcm qcm;

    @BeforeClass
    public static void initDAOContext() {
        DAOContext.init();

        qcm = new Qcm();
        QcmDAO.createOrUpdate(qcm);
    }

    @Test
    public void save() {
        // Given
        Question question = new Question();
        question.setQcm(qcm);
        question.setContent("test");

        // When
        QuestionDAO.createOrUpdate(question);

        // Then
        Assert.assertNotNull(question.getId());
    }

    @Test
    public void update() {
        // Given
        Question question = new Question();
        question.setQcm(qcm);
        question.setContent("test");
        QuestionDAO.createOrUpdate(question);

        Assert.assertNotNull(question.getId());

        // When
        question.setContent("nouveau");
        QuestionDAO.createOrUpdate(question);

        // Then
        Assert.assertNotNull(question);
        Assert.assertNotEquals(question.getContent(),"test");
    }

    @Test
    public void delete() {
        // Given
        Question question = new Question();
        question.setQcm(qcm);
        question.setContent("test");
        QuestionDAO.createOrUpdate(question);

        Assert.assertNotNull(question.getId());
        Long id = question.getId();

        // When
        QuestionDAO.delete(question);

        // Then
        Assert.assertNull(QuestionDAO.getById(id));

    }

    @Test
    public void getById() {
        // Given
        Question question = new Question();
        question.setQcm(qcm);
        question.setContent("test");

        // When
        QuestionDAO.createOrUpdate(question);

        // Then
        Assert.assertNotNull(question.getId());

        Assert.assertNull(QuestionDAO.getById(0L));
        Assert.assertNotNull(QuestionDAO.getById(question.getId()));
    }

    @AfterClass
    public static void removeQcm() {
        QcmDAO.delete(qcm);
    }
}