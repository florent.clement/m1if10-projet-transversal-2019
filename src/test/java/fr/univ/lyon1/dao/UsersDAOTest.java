package fr.univ.lyon1.dao;

import fr.univ.lyon1.model.Users;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UsersDAOTest {

    @Before
    public void setUp() throws Exception {
        DAOContext.init();
    }

    @Test
    public void create() {
        // Given
        Users testUsers = new Users("testUSERSDAOCREATE", 123);
        Users testUsers2 = new Users("testUSERSDAOCREATE",123);

        // When
        boolean test1 = UsersDAO.create(testUsers);
        boolean test2 = UsersDAO.create(testUsers2);

        // Then
        Assert.assertTrue(test1);
        Assert.assertFalse(test2);

        // Clean Up
        UsersDAO.delete(testUsers);
        Assert.assertNull(UsersDAO.getByName(testUsers.getName()));
    }

    @Test
    public void delete() {
        // Given
        Users testUsers = new Users("testUSERSDAODELETE",123);

        // When
        boolean test = UsersDAO.create(testUsers);
        Assert.assertTrue(test);
        UsersDAO.delete(testUsers);

        // Then
        Assert.assertNull(UsersDAO.getByName(testUsers.getName()));
    }

    @Test
    public void getByName() {
        // Given
        Users testUsers = new Users("testUSERSDAOGET",123);

        // When
        boolean test = UsersDAO.create(testUsers);
        Assert.assertTrue(test);

        // Then
        Assert.assertEquals(testUsers, UsersDAO.getByName(testUsers.getName()));

        //Clean Up
        UsersDAO.delete(testUsers);
        Assert.assertNull(UsersDAO.getByName(testUsers.getName()));
    }
}