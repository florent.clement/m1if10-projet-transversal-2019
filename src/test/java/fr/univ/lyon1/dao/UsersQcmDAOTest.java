package fr.univ.lyon1.dao;

import fr.univ.lyon1.model.Qcm;
import fr.univ.lyon1.model.Users;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class UsersQcmDAOTest {

    @Before
    public void setUp() throws Exception {
        DAOContext.init();
    }

    @Test
    public void saveTuple() {
        // Given
        Users testUsers = new Users("testUSERSQCMDAO", 123);
        Assert.assertTrue(UsersDAO.create(testUsers));
        Assert.assertEquals(testUsers, UsersDAO.getByName(testUsers.getName()));
        Qcm testQcm = new Qcm();
        QcmDAO.createOrUpdate(testQcm);
        Assert.assertEquals(testQcm, QcmDAO.getById(testQcm.getId()));

        // When
        boolean res = UsersQcmDAO.saveTuple(testUsers.getName(), testQcm.getId());

        //Then
        Assert.assertTrue(res);

        //Clean Up
        UsersDAO.delete(testUsers);
        Assert.assertNull(UsersDAO.getByName(testUsers.getName()));
        QcmDAO.delete(testQcm);
        Assert.assertNull(QcmDAO.getById(testQcm.getId()));
    }

    @Test
    public void getQcmIdList() {
        // Given
        Users testUsers = new Users("testUSERSQCMDAO", 123);
        Assert.assertTrue(UsersDAO.create(testUsers));
        Assert.assertEquals(testUsers, UsersDAO.getByName(testUsers.getName()));
        ArrayList<Qcm> testQcmList = new ArrayList<>();
        Qcm testQcm = null;
        for (int i = 0; i < 10; ++i) {
            testQcm = new Qcm();
            QcmDAO.createOrUpdate(testQcm);
            Assert.assertEquals(testQcm, QcmDAO.getById(testQcm.getId()));
            testQcmList.add(testQcm);
        }
        boolean test = true;

        // When
        for (Qcm iterator : testQcmList) {
            test = test && UsersQcmDAO.saveTuple(testUsers.getName(), iterator.getId());
        }
        Assert.assertTrue(test);
        List<Qcm> res = UsersQcmDAO.getQcmIdList(testUsers.getName());

        // Then
        Assert.assertNotNull(res);
        ArrayList<Long> ids = new ArrayList<>();
        ArrayList<String> titles = new ArrayList<>();
        for (Qcm qcm : res) {
            ids.add(qcm.getId());
            titles.add(qcm.getTitre());
        }
        for (Qcm iterator : testQcmList) {
            Assert.assertTrue(ids.contains(iterator.getId()));
            Assert.assertTrue(titles.contains(iterator.getTitre()));
        }


        // Clean Up
        UsersDAO.delete(testUsers);
        Assert.assertNull(UsersDAO.getByName(testUsers.getName()));
        for (Qcm iterator : testQcmList) {
            QcmDAO.delete(iterator);
            Assert.assertNull(QcmDAO.getById(iterator.getId()));
        }
    }

    @Test
    public void getUserName() {
        // Given
        Users testUsers = new Users("testUSERSQCMDAO", 123);
        Assert.assertTrue(UsersDAO.create(testUsers));
        Assert.assertEquals(testUsers, UsersDAO.getByName(testUsers.getName()));
        Qcm testQcm = new Qcm();
        QcmDAO.createOrUpdate(testQcm);
        Assert.assertEquals(testQcm, QcmDAO.getById(testQcm.getId()));
        boolean test = UsersQcmDAO.saveTuple(testUsers.getName(), testQcm.getId());
        Assert.assertTrue(test);

        // When
        String res = UsersQcmDAO.getUserName(testQcm.getId());

        // Then
        Assert.assertNotNull(res);
        Assert.assertEquals(testUsers.getName(), res);

        //Clean Up
        UsersDAO.delete(testUsers);
        Assert.assertNull(UsersDAO.getByName(testUsers.getName()));
        QcmDAO.delete(testQcm);
        Assert.assertNull(QcmDAO.getById(testQcm.getId()));
    }
}