package fr.univ.lyon1.dao;

import fr.univ.lyon1.model.Qcm;
import fr.univ.lyon1.model.Question;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class QcmDAOTest {

    @BeforeClass
    public static void initDTOContext() {
        DAOContext.init();
    }

    @Test
    public void save() {
        // Given
        Qcm qcm = new Qcm();
        qcm.addQuestion(new Question());
        qcm.addQuestion(new Question());

        // When
        QcmDAO.createOrUpdate(qcm);

        // Then
        Assert.assertNotNull(qcm.getId());
        Assert.assertEquals(2, qcm.getQuestionList().size());
  }

    @Test
    public void update() {
        // Give
        Qcm qcm = new Qcm();
        qcm.setTitre("titre1");
        QcmDAO.createOrUpdate(qcm);

        // When
        qcm.setTitre("titre1");
        QcmDAO.createOrUpdate(qcm);

        // Then
        Qcm qcm2 = QcmDAO.getById(qcm.getId());
        Assert.assertNotNull(qcm2);
        Assert.assertEquals(qcm.getTitre(),qcm2.getTitre());
    }

    @Test
    public void delete() {
        // Given
        Qcm qcm = new Qcm();
        QcmDAO.createOrUpdate(qcm);
        Assert.assertNotNull(qcm.getId());
        Long id = qcm.getId();

        // When
        QcmDAO.delete(qcm);

        // Then
        Assert.assertNull(QcmDAO.getById(id));

    }

    @Test
    public void getById() {
        // Given
        Qcm qcm = new Qcm();

        // When
        QcmDAO.createOrUpdate(qcm);

        // Then
        Assert.assertNotNull(qcm.getId());

        Assert.assertNull(QcmDAO.getById(0L));
        Assert.assertNotNull(QcmDAO.getById(qcm.getId()));
    }
}