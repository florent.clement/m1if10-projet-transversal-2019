package fr.univ.lyon1.context;

import fr.univ.lyon1.dto.ChoiceDTO;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import static org.junit.Assert.*;

public class JsonStrategyTest {

    @Test
    public void getContentPage() {
        // Given
        JsonStrategy jsonStrategy = new JsonStrategy();
        ChoiceDTO choiceDTO = Mockito.mock(ChoiceDTO.class);
        JsonObject jsonObject = Json.createObjectBuilder().build();
        Mockito.when(choiceDTO.getJsonStructure()).thenReturn(jsonObject);

        // When
        String content = jsonStrategy.getContentPage(choiceDTO);

        // Then
        Assert.assertEquals(content,jsonObject.toString());
}

    @Test
    public void parseRequest() {
        // Given
        JsonStrategy jsonStrategy = new JsonStrategy();
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();

        jsonObjectBuilder.add("id",1);
        jsonObjectBuilder.add("content","content");
        jsonObjectBuilder.add("counter",0);

        JsonObject jsonObject = jsonObjectBuilder.build();
        ChoiceDTO choiceDTO = new ChoiceDTO().parseJson(jsonObject);

        // When
        String content = jsonStrategy.getContentPage(choiceDTO);

        Assert.assertEquals(jsonObject.toString() , content);
    }
}