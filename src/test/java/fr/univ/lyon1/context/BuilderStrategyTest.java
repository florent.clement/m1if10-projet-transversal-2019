package fr.univ.lyon1.context;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class BuilderStrategyTest {

    @Test
    public void getStrategy() {

        String contentType;
        ResourcesStrategy resourcesStrategy;

        // Test XMLStrategy
        contentType = "application/xml";
        resourcesStrategy = BuilderStrategy.getStrategy(contentType);

        Assert.assertNotNull(resourcesStrategy);
        Assert.assertEquals( XmlStrategy.class,
                BuilderStrategy.getStrategy(contentType).getClass());

        // Test JsonStrategy
        contentType = "application/json";
        resourcesStrategy = BuilderStrategy.getStrategy(contentType);

        Assert.assertNotNull(resourcesStrategy);
        Assert.assertEquals( JsonStrategy.class,
                BuilderStrategy.getStrategy(contentType).getClass());

        // Test FormData
        contentType = "application/x-www-form-urlencoded";
        resourcesStrategy = BuilderStrategy.getStrategy(contentType);

        Assert.assertNotNull(resourcesStrategy);
        Assert.assertEquals( FormDataStrategy.class,
                BuilderStrategy.getStrategy(contentType).getClass());

        // Autre
        contentType = "test";
        resourcesStrategy = BuilderStrategy.getStrategy(contentType);

        Assert.assertNull(resourcesStrategy);
    }
}