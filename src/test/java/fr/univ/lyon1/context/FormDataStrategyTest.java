package fr.univ.lyon1.context;

import fr.univ.lyon1.dto.ChoiceDTO;
import fr.univ.lyon1.model.Choice;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import javax.json.Json;
import javax.json.JsonObject;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class FormDataStrategyTest {

    @Test
    public void getContentPage() {
        // Given
        FormDataStrategy formDataStrategy = new FormDataStrategy();

        // Then
        Assert.assertNull(formDataStrategy.getContentPage(null));
        Assert.assertNull(formDataStrategy.getContentPage(new ChoiceDTO()));
    }

    @Test
    public void parseRequest() {
        // Given
        FormDataStrategy formDataStrategy = new FormDataStrategy();

        String request = "id=1&content=content&counter=0";
        ChoiceDTO choiceDTO = new ChoiceDTO();

        // When
        formDataStrategy.parseRequest(request,choiceDTO);

        Choice choice = choiceDTO.build();
        // Then
        Assert.assertNotNull(choiceDTO);
        Assert.assertNotNull(choice);
        Assert.assertEquals((Long) 1L, choice.getId());
        Assert.assertEquals("content", choice.getContent());
        Assert.assertEquals(0, choice.getCounter());
    }
}