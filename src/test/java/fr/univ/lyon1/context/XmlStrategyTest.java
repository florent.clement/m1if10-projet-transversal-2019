package fr.univ.lyon1.context;

import fr.univ.lyon1.dto.BuilderDTO;
import fr.univ.lyon1.dto.QcmDTO;
import fr.univ.lyon1.model.Qcm;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

public class XmlStrategyTest {

    @Test
    public void getContentPage() {
        // Given
        XmlStrategy strat = new XmlStrategy();
        Qcm qcm = new Qcm();
        qcm.setTitre("titre");
        qcm.setId(0L);
        QcmDTO qcmDTO = BuilderDTO.buildDTO(qcm, QcmDTO.class);

        // Then
        Assert.assertNotNull(strat.getContentPage(new QcmDTO()));
        Assert.assertNotNull(strat.getContentPage(qcmDTO));
    }

    @Test
    public void parseRequest() {
        XmlStrategy strat = new XmlStrategy();
        Qcm qcm = new Qcm();
        qcm.setTitre("titre");
        qcm.setId(0L);
        QcmDTO qcmDTO = BuilderDTO.buildDTO(qcm, QcmDTO.class);
        QcmDTO qcmDTO2 = new QcmDTO();

        String res = strat.getContentPage(qcmDTO);
        strat.parseRequest(res, qcmDTO2);

        Assert.assertEquals(qcm, qcmDTO2.build());
    }
}