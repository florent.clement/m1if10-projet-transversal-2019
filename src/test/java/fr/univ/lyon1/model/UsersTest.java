package fr.univ.lyon1.model;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class UsersTest {

    @Test
    public void getName() {
        // Given
        Users users = new Users("name",0);
        
        // Then
        Assert.assertEquals("name", users.getName());

    }

    @Test
    public void setName() {
        // Given
        Users users = new Users();

        Assert.assertNull(users.getName());

        // When
        users.setName("name");

        // Then
        Assert.assertEquals("name", users.getName());
    }

    @Test
    public void getPassword() {
        //TODO
    }

    @Test
    public void setPassword() {
        //TODO
    }

    @Test
    public void testHashCode() {
        //TODO
    }

    @Test
    public void testEquals() {
        //TODO
    }
}