package fr.univ.lyon1.model;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class QuestionTest {

    @Test
    public void addChoice() {
        // Given
        Question question = new Question();
        Assert.assertEquals(0, question.getChoiceList().size());
        Choice choice = new Choice();

        // When
        question.addChoice(choice);

        // Then
        Assert.assertEquals(1, question.getChoiceList().size());
        Assert.assertEquals(choice, question.getChoiceList().get(0));
    }

    @Test
    public void getChoiceList() {
        // Given
        List<Choice> choiceList = new ArrayList<>();
        Question question = new Question(null,"content",choiceList);

        // Then
        Assert.assertEquals(choiceList, question.getChoiceList());
    }

    @Test
    public void getContent() {
        // Given
        Question question = new Question(null,"content");

        // Then
        Assert.assertEquals("content", question.getContent());
    }

    @Test
    public void setContent() {
        // Given
        Question question = new Question();

        Assert.assertEquals("", question.getContent());
        // When
        question.setContent("content");

        // Then
        Assert.assertEquals("content", question.getContent());
    }

    @Test
    public void getQcm() {
        // Given
        Qcm qcm = new Qcm();
        Question question = new Question(qcm,"content");

        // Then
        Assert.assertEquals(qcm, question.getQcm());
    }

    @Test
    public void setQcm() {
    }

    @Test
    public void getId() {
        // Given
        Question question = new Question();

        // Then
        Assert.assertNull(question.getId());
    }

    @Test
    public void setId() {
        // Given
        Question question = new Question();

        // When
        question.setId(2L);

        // Then
        Assert.assertEquals((Long)2L, question.getId());
    }

    @Test
    public void setChoiceList() {
        // Given
        List<Choice> choiceList = new ArrayList<>();
        Question question = new Question();

        // When
        question.setChoiceList(choiceList);

        // Then
        Assert.assertEquals(choiceList, question.getChoiceList());
    }

    @Test
    public void testHashCode() {
        //TODO
    }

    @Test
    public void testEquals() {
        //TODO
    }
}