package fr.univ.lyon1.model;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class QcmTest {

    @Test
    public void getId() {
        // Given
        Qcm qcm = new Qcm();

        // Then
        Assert.assertNull(qcm.getId());
    }

    @Test
    public void setId() {
        // Given
        Qcm qcm = new Qcm();

        // When
        qcm.setId(2L);

        // Then
        Assert.assertEquals((Long)2L, qcm.getId());
    }

    @Test
    public void getTitre() {
        // Given
        Qcm qcm = new Qcm("titre");

        // Then
        Assert.assertEquals("titre", qcm.getTitre());
    }

    @Test
    public void setTitre() {
        // Given
        Qcm qcm = new Qcm();

        Assert.assertEquals("", qcm.getTitre());
        // When
        qcm.setTitre("test");

        // Then
        Assert.assertEquals("test", qcm.getTitre());
    }

    @Test
    public void getQuestionList() {
        // Given
        List<Question> questionList = new ArrayList<>();
        Qcm qcm = new Qcm("test",questionList);

        // Then
        Assert.assertEquals(questionList, qcm.getQuestionList());
    }

    @Test
    public void addQuestion() {
        // Given
        Qcm qcm = new Qcm();
        Assert.assertEquals(0, qcm.getQuestionList().size());
        Question question = new Question();

        // When
        qcm.addQuestion(question);

        // Then
        Assert.assertEquals(1, qcm.getQuestionList().size());
        Assert.assertEquals(question, qcm.getQuestionList().get(0));
    }

    @Test
    public void setQuestionList() {
        // Given
        List<Question> questionList = new ArrayList<>();
        Qcm qcm = new Qcm();

        // When
        qcm.setQuestionList(questionList);

        // Then
        Assert.assertEquals(questionList, qcm.getQuestionList());
    }

    @Test
    public void testHashCode() {
        //TODO
    }

    @Test
    public void testEquals() {
        //TODO
    }
}