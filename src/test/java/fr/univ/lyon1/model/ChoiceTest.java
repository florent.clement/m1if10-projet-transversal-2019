package fr.univ.lyon1.model;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class ChoiceTest {

    @Test
    public void getId() {
        // Given
        Choice choice = new Choice();

        // Then
        Assert.assertNull(choice.getId());
    }

    @Test
    public void setId() {
        // Given
        Choice choice = new Choice();

        // When
        choice.setId(2L);

        // Then
        Assert.assertEquals((Long)2L, choice.getId());
    }

    @Test
    public void getContent() {
        // Given
        Choice choice = new Choice(null,"content",0);

        // Then
        Assert.assertEquals("content", choice.getContent());
    }

    @Test
    public void setContent() {
        // Given
        Choice choice = new Choice(null,"content",0);

        Assert.assertEquals("content", choice.getContent());
        // When
        choice.setContent("test");

        // Then
        Assert.assertEquals("test", choice.getContent());
    }

    @Test
    public void getCounter() {
        // Given
        Choice choice = new Choice(null,"content",0);

        // Then
        Assert.assertEquals(0, choice.getCounter());
    }

    @Test
    public void setCounter() {
        // Given
        Choice choice = new Choice(null,"content",0);

        Assert.assertEquals(0, choice.getCounter());
        // When
        choice.setCounter(5054);

        // Then
        Assert.assertEquals(5054, choice.getCounter());
    }

    @Test
    public void getQuestion() {
        // Given
        Question question = new Question();
        Choice choice = new Choice(question,"content",0);

        Assert.assertEquals(question,choice.getQuestion());
    }

    @Test
    public void setQuestion() {
        // Given
        Question question = new Question();
        question.setContent("content");
        Choice choice = new Choice(question,"content",0);


        Assert.assertEquals(question,choice.getQuestion());
        Question questionTest = new Question();

        // When
        choice.setQuestion(questionTest);

        // Then
        Assert.assertNotNull(questionTest);
        Assert.assertNotEquals(question,questionTest);
        Assert.assertNotEquals(question, choice.getQuestion());
    }

    @Test
    public void incrementCounter() {
        // Given
        Choice choice = new Choice(null,"content",0);
        Assert.assertEquals(0, choice.getCounter());

        // When
        choice.incrementCounter();

        // Then
        Assert.assertEquals(1, choice.getCounter());
    }

    @Test
    public void testHashCode() {
        //TODO
    }

    @Test
    public void testEquals() {
        //TODO
    }
}